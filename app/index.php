<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once($_SERVER['DOCUMENT_ROOT'] . '/shared/includes/header.php'); ?>
</head>
<body>
    <section id="headsUpContainer">
        <div id="headsUpDisplay">
            <?php require_once($_SERVER['DOCUMENT_ROOT'] . '/shared/includes/headsUpNav.php'); ?>
            <?php require_once($_SERVER['DOCUMENT_ROOT'] . '/shared/includes/hotbar.php'); ?>
        </div>
    </section>
    <?php require_once($_SERVER['DOCUMENT_ROOT'] . '/shared/includes/nav.php'); ?>
    <?php require_once($_SERVER['DOCUMENT_ROOT'] . '/shared/includes/toolBar.php'); ?>
    <div class="container mt-5 mapContainerOverride">
        <?php require_once($_SERVER['DOCUMENT_ROOT'] . '/routes/routes.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'] . '/shared/includes/footer.php'); ?>
</body>
</html>