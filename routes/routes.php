<?php
$request = strtok($_SERVER['REQUEST_URI'], '?');
$dirname = dirname(__DIR__, 1) . '/views/';

switch ($request) {
    case '/':case '':case '/home':
        require $dirname . 'home/index.php';
        break;
    case '/game':
        require $dirname . 'game/index.php';
        break;
    case '/help':
        require $dirname . 'help/index.php';
        break;
    case '/register':
        require $dirname . 'user/register.php';
        break;
    case '/login':
        require $dirname . 'user/login.php';
        break;
    case '/logout':
        require $dirname . 'user/logout.php';
        break;
    case '/load_locations':
        require $dirname . 'utilities/load_locations.php';
        break;
    case '/view_locations':
        require $dirname . 'utilities/view_locations.php';
        break;
    case '/add_resources':
        require $dirname . 'utilities/add_resources.php';
        break;
    case '/add_locations':
        require $dirname . 'utilities/add_locations.php';
        break;
    case '/utility_dashboard':
        require $dirname . 'utilities/utility_dashboard/utility_dashboard.php';
        break;
    default:
        http_response_code(404);
        require $dirname . 'errors/404.php';
        break;
}