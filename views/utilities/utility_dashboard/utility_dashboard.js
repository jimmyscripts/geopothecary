document.onclick = async function(event) {
    if(event.target.id == 'runDefaultListBuilder') {
        const data = {
            action:'run-default-list-builder',
            controller:'UtilityController'
        }
        const response = await postMethod(data);
        if(!checkResponseErrors(response)){
            return;
        }
    }

    if(event.target.id == 'runDefaultLocationBuilder') {
        const data = {
            action:'run-default-location-builder',
            controller:'UtilityController'
        }
        const response = await postMethod(data);
        if(!checkResponseErrors(response)){
            return;
        }
    }

    if(event.target.id == 'runDefaultSourceBuilder') {
        const data = {
            action:'run-default-source-builder',
            controller:'UtilityController'
        }
        const response = await postMethod(data);
        if(!checkResponseErrors(response)){
            return;
        }
    }
}

const postMethod = async (data) => {
    const url = `/shared/post.php`;
    const response = await fetch(url, {
        method: 'POST',
        mode: "same-origin",
        credentials: 'same-origin',
        headers: {'Content-Type':'application/json'},
        body: JSON.stringify(data)
    });
    return response;
}