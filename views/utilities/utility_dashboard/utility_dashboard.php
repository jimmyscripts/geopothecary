<?php

?>
<script src="/views/utilities/utility_dashboard/utility_dashboard.js"></script>
<section class="container">
    <div class="row">
        <div class="col">
            <button type="button" class="btn btn-primary" id="runDefaultListBuilder">Run Default List Builder</button>
        </div>
        <div class="col">
            <button type="button" class="btn btn-primary" id="runDefaultLocationBuilder">Run Default Location Builder</button>
        </div>
        <div class="col">
            <button type="button" class="btn btn-primary" id="runDefaultSourceBuilder">Run Default Source Builder</button>
        </div>
    </div>
</section>
