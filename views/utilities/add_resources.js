document.onclick = async function(event) {
    if(event.target.id == 'submitAddItemToSource') {
        const addItemToSourceForm = document.getElementById('addItemToSourceForm' + event.target.value);
        const formData = new FormData(addItemToSourceForm);

        let object = {};
        formData.forEach((value, key) => object[key] = value);

        const data = {
            action:'add-new-resource',
            controller:'UtilityController',
            output:'JSON',
            data: object
        }

        const url = `/shared/post.php`;
        const response = await fetch(url, {
            method: 'POST',
            mode: "same-origin",
            credentials: 'same-origin',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify(data)
        });
        if(!checkResponseErrors(response)){
            return;
        }
    }
}