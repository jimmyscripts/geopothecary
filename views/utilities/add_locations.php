<?php

$utility = new Utility();
$locations = $utility->getLocations();
$sources = $utility->getSources();
$items = $utility->getItems();

$fullSources = [];
foreach($locations as $location) {
    $locationSources = array_filter($sources, function($source) use ($location) {
        return $location['location_id'] == $source['location_id'];
    });
    $fullSources[$location['location_id']] = $location;
    $fullSources[$location['location_id']]['sources'] = $locationSources;
}
$fullSources = array_values($fullSources);
?>
<script src="/views/utilities/add_locations.js"></script>
<section class="mb-5">
    <h4>Add Locations</h4>
    <form class="row" id="addLocationForm">
        <div class="col-4">
            <label for="location_lat">Location Lat</label>
            <input class="form-control" type="text" name="location_lat"/>
        </div>
        <div class="col-4">
            <label for="location_long">Location Long</label>
            <input class="form-control" type="text" name="location_long"/>
        </div>
        <div class="col-4">
            <label for="location_name">Location Name</label>
            <input class="form-control" type="text" name="location_name"/>
        </div>
        <div class="col-8">
            <label for="location_address">Location Address</label>
            <input class="form-control" type="text" name="location_address"/>
        </div>
        <div class="col-4">
            <label for="submitLocation">Submit Location</label>
            <input class="btn btn-primary form-control" type="button" id="submitLocation" value="Add Location"/>
        </div>
    </form>
</section>
<?php foreach($fullSources as $fullSource): ?>
    <h3><?= $fullSource['location_id'] ?>: <?= $fullSource['location_name'] ?: $fullSource['google_name'] ?></h3>
    <p>
        <?php foreach($fullSource['sources'] as $thisSource): ?>
            <?= $items[$thisSource['item_id']]['item_id'] ?>: <?= $items[$thisSource['item_id']]['item_name'] ?>
            <br />
        <?php endforeach; ?>
    </p>
<?php endforeach; ?>

