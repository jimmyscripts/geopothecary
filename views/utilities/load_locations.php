<?php
echo 'Loading Locations...';
echo '<br />';

$zip = $_GET['zip'];
$query = $_GET['query'];
$type = $_GET['type'];

$zipIteration = 0;
$queryIteration = 0;
$typeIteration = 0;

$exitedScript = false;
//last http://localhost:8010/load_locations?zip=32222&query=pier&type=rv%20park
$zipArray = [
    32073, 32202, 32204, 32205, 32206, 32207, 32208, 32209, 32210, 32211, 32212, 32216, 32217, 32218, 32219,
    32220, 32221, 32222, 32223, 32224, 32225, 32226, 32227, 32228, 32233, 32234, 32244, 32246, 32250, 32254, 32256,
    32257, 32258, 32277
];
$queryArray = [
    'farm', 'garden', 'aquarium', 'pond', 'playground', 'park', 'lake', 'river', 'trail', 'historic',
    'school', 'university', 'library', 'market', 'restaurant', 'creek', 'dock', 'marina', 'fishing',
    'bridge', 'ferry', 'preserve', 'reserve', 'camp site', 'cemetery', 'grocery store', 'woods', 'haunted house',
    'pier', 'resort', 'marsh', 'cabin'
];
$typeArray = [
    'park', 'zoo', 'amusement park', 'tourist attraction', 'rv park', 'store', 'natural feature', 'school',
    'university', 'aquarium', 'library'
];

if($zip && $query && $type) {
    $zipIteration = array_search($zip, $zipArray);
    $queryIteration = array_search($query, $queryArray);
    $typeIteration = array_search($type, $typeArray);

    $typeIteration++;

    if($typeIteration > count($typeArray) - 1) {
        $queryIteration++;
        $typeIteration = 0;
    }

    if($queryIteration > count($queryArray) - 1) {
        $zipIteration++;
        $queryIteration = 0;
    }

    if($zipIteration > count($zipArray) - 1) {
        $exitedScript = true;
        exit();
    }

    $zip = $zipArray[$zipIteration];
    $query = $queryArray[$queryIteration];
    $type = $typeArray[$typeIteration];
} else {
    $zip = $zipArray[$zipIteration];
    $query = $queryArray[$queryIteration];
    $type = $typeArray[$typeIteration];
}



echo 'Query: ' . $query ?: 'Missing Query';
echo '<br />';
echo 'Zip: ' . $zip ?: 'Missing Zip';
echo '<br />';
echo 'Type: ' . $type ?: 'Missing Type';
echo '<br /><br />';

?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDkWNfjlJ11Kv2HLn5axd2Yxp6yz1sGZns&callback=initMap&libraries=places&v=weekly"
        defer></script>
<style>
    #map {
        height: 400px;
        width: 100%;
    }
</style>
<div id="map"></div>
<div id="google_places_list"></div>
<script defer>
    const updateURLParameter = (url, param, paramVal) => {
        var newAdditionalURL = "";
        var tempArray = url.split("?");
        var baseURL = tempArray[0];
        var additionalURL = tempArray[1];
        var temp = "";
        if (additionalURL) {
            tempArray = additionalURL.split("&");
            for (var i=0; i<tempArray.length; i++){
                if(tempArray[i].split('=')[0] != param){
                    newAdditionalURL += temp + tempArray[i];
                    temp = "&";
                }
            }
        }

        var rows_txt = temp + "" + param + "=" + paramVal;
        return baseURL + "?" + newAdditionalURL + rows_txt;
    }

    let map, infoWindow, service, geocoder, posit;

    function initMap() {

        map = new google.maps.Map(document.getElementById("map"), {
            center: {lat: -33.8688, lng: 151.2195},
            zoom: 13,
            mapTypeId: "roadmap",
        });

        infoWindow = new google.maps.InfoWindow();

        geocoder = new google.maps.Geocoder();

        const address = '<?php echo $zip; ?>';
        const querySearched = '<?php echo $query; ?>';
        const typeSearched = '<?php echo $type; ?>';

        let newURL = updateURLParameter(window.location.href, 'locId', 'newLoc');
        newURL = updateURLParameter(newURL, 'resId', 'newResId');
        window.history.replaceState('', '', updateURLParameter(window.location.href, "zip", address));
        window.history.replaceState('', '', updateURLParameter(window.location.href, "query", querySearched));
        window.history.replaceState('', '', updateURLParameter(window.location.href, "type", typeSearched));

        const querySelected = getQuerySelected() + '%' + address;
        const typeSelected = '<?php echo str_replace('_', ' ', $type); ?>';
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == 'OK') {
                map.setCenter(results[0].geometry.location);
                const marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
                posit = results[0].geometry.location;

                const pos = {
                    lat: posit.lat(),
                    lng: posit.lng(),
                };

                infoWindow.setPosition(pos);
                infoWindow.setContent("Location found.");
                //infoWindow.open(map);
                map.setCenter(pos);


                const request = {
                    location: pos,
                    radius: '50',
                    query: querySelected,//playground+32256 works as a query
                    type: typeSelected
                };

                service = new google.maps.places.PlacesService(map);
                service.textSearch(request, callback);
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    const callback = (results, status) => {
        const list = document.getElementById('google_places_list');
        console.log(results);
        if (status == google.maps.places.PlacesServiceStatus.OK) {
            let template =``;
            for (let i = 0; i < results.length; i++) {
                let place = results[i];
                const groups = ['Best Rated', 'Most Rated', 'New', 'Others Nearby'];

                template += `<a href="https://www.google.com/maps/place/?q=place_id:${place.place_id}" target="_blank"><h4> ${place.name} </h4></a>`;
                template += `<p> ${place.formatted_address} </p>`;
                template += `<p>Google Rating: ${place.rating.toFixed(1)} out of ${place.user_ratings_total} ${(place.user_ratings_total == 1 ? "review" : "reviews")}. </p>`;
                template += `<br>`;
                list.innerHTML = template;
                createMarker(place);
            }
        }
    }

    const createMarker = async (place) => {
        const lat = parseFloat(place.geometry.location.lat().toFixed(11));
        const lng = parseFloat(place.geometry.location.lng().toFixed(11));
        const googleAddress = place.formatted_address;
        const googleName = place.name;
        const googleType = JSON.stringify(place.types);
        const querySelected = getQuerySelected();

        let dataValues = {
            'location_lat' : lat,
            'location_long' : lng,
            'google_name' : googleName,
            'google_address' : googleAddress,
            'google_types' : googleType,
            'google_queries' : querySelected
        };

        const data = {
            action:'create-place',
            controller:'UtilityController',
            output:'JSON',
            role_required: 'admin',
            session_role: '<?= $_SESSION['user_role'] ?>',
            data: dataValues
        };

        const url = `/shared/post.php`;
        const response = await fetch(url, {
            method: 'POST',
            mode: "same-origin",
            credentials: 'same-origin',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify(data)
        });
        if(!checkResponseErrorsModal(response)){
            return;
        }

        const marker = new google.maps.Marker({
            map,
            position: place.geometry.location,
        });
        google.maps.event.addListener(marker, "click", () => {
            infoWindow.setContent(place.name);
            infoWindow.open(map);
        });
    }

    const getQuerySelected = () => {
        return '<?php echo str_replace('_', ' ', $query); ?>';
    }

    document.addEventListener("DOMContentLoaded", () => {
        setTimeout(() => {
            location.reload();
        }, 5000);
    });
</script>
