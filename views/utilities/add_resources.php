<?php

$utility = new Utility();
$locations = $utility->getLocations();
$sources = $utility->getSources();
$items = $utility->getItems();

$fullSources = [];
foreach($locations as $location) {
    $locationSources = array_filter($sources, function($source) use ($location) {
        return $location['location_id'] == $source['location_id'];
    });
    $fullSources[$location['location_id']] = $location;
    $fullSources[$location['location_id']]['sources'] = $locationSources;
}
$fullSources = array_values($fullSources);
?>
<script src="/views/utilities/add_resources.js"></script>
<?php foreach($fullSources as $fullSource): ?>
    <h4><?= $fullSource['location_name'] ?: $fullSource['google_name'] ?> (<?= $fullSource['location_id'] ?>)</h4>
    <p>
        <div class="row">
            <?php foreach($fullSource['sources'] as $thisSource): ?>
                <div class="col-1 text-center mb-3">
                    <img src="<?= '/shared/svg/' . $items[$thisSource['item_id']]['item_slug'] . '.svg' ?>" width="45px">
                    <br/>
                    <?= $items[$thisSource['item_id']]['item_name'] ?>
                    <br/>
                    (<?= $items[$thisSource['item_id']]['item_id'] ?>)
                    <br/>
                    Count: <?= $thisSource['source_count'] ?>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="mb-5">
            <form id="addItemToSourceForm<?= $fullSource['location_id'] ?>">
                <input type="hidden" value="<?= $fullSource['location_id'] ?>" name="locationId"/>
                <div class="row">
                    <div class="col-3">
                        <label for="itemSelect">Select Item</label>
                        <select class="form-control" name="itemSelect">
                            <option value=""></option>
                            <?php foreach($items as $item): ?>
                                <option value="<?= $item['item_id'] ?>"><?= $item['item_name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-3">
                        <label for="itemCount">Add Count</label>
                        <input class="form-control" type="number" name="itemCount" />
                    </div>
                    <div class="col-3">
                        <label for="itemCount">Add Rarity (1 = Common, 10 = Rare)</label>
                        <input class="form-control" type="number" name="rarity" />
                    </div>
                    <div class="col-3 d-flex align-items-end">
                        <button class="btn btn-primary" id="submitAddItemToSource" type="button" value="<?= $fullSource['location_id'] ?>">+ Add</button>
                    </div>
                </div>
            </form>
        </div>
    </p>
<?php endforeach; ?>