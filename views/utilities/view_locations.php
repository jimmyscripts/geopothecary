<?php

$utility = new Utility();
$locations = $utility->getLocations();
$sources = $utility->getSources();
$items = $utility->getItems();

$fullSources = [];
foreach($locations as $location) {
    $locationSources = array_filter($sources, function($source) use ($location) {
        return $location['location_id'] == $source['location_id'];
    });
    $fullSources[$location['location_id']] = $location;
    $fullSources[$location['location_id']]['sources'] = $locationSources;
}
$fullSources = array_values($fullSources);
?>
<?php foreach($fullSources as $fullSource): ?>
    <h3><?= $fullSource['location_id'] ?>: <?= $fullSource['location_name'] ?: $fullSource['google_name'] ?></h3>
    <p>
        <?php foreach($fullSource['sources'] as $thisSource): ?>
            <?= $items[$thisSource['item_id']]['item_id'] ?>: <?= $items[$thisSource['item_id']]['item_name'] ?>
            <br />
        <?php endforeach; ?>
    </p>
<?php endforeach; ?>

