document.addEventListener('click', async e => {
    if(e.target.id == 'registerUserSubmit') {
        const registerForm = document.getElementById('registerForm');
        const formData = new FormData(registerForm);
        let dataValues = {};
        formData.forEach((value, key) => dataValues[key] = value);

        const data = {
            action:'register-user',
            controller:'UserController',
            output:'JSON',
            data: JSON.stringify(dataValues)
        }

        const url = `/shared/post.php`;
        const response = await fetch(url, {
            method: 'POST',
            mode: "same-origin",
            credentials: 'same-origin',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify(data)
        });
        if(!checkResponseErrors(response)){
            return;
        }

        const registrationResponse = await response.json();
        const regResponse = registrationResponse.responseData;
        console.log(regResponse)
    }

    if(e.target.id == 'loginUserSubmit') {
        const loginForm = document.getElementById('loginForm');
        const formData = new FormData(loginForm);
        let dataValues = {};
        formData.forEach((value, key) => dataValues[key] = value);

        const data = {
            action:'login-user',
            controller:'UserController',
            output:'JSON',
            data: JSON.stringify(dataValues)
        }

        const url = `/shared/post.php`;
        const response = await fetch(url, {
            method: 'POST',
            mode: "same-origin",
            credentials: 'same-origin',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify(data)
        });
        if(!checkResponseErrors(response)){
            return;
        }

        window.location.reload();
    }
});

window.onload = async () => {
    if(window.location.pathname == '/logout' && window.location.search == '?logout=logout') {
        const dataValues = {logout: 'logout'};
        const data = {
            action:'logout-user',
            controller:'UserController',
            output:'JSON',
            data: JSON.stringify(dataValues)
        }

        const url = `/shared/post.php`;
        const response = await fetch(url, {
            method: 'POST',
            mode: "same-origin",
            credentials: 'same-origin',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify(data)
        });
        if(!checkResponseErrors(response)){
            return;
        }

        let searchParams = new URLSearchParams(window.location.search);
        searchParams.set("logout", "complete");
        window.location.search = searchParams.toString();
    }
}
