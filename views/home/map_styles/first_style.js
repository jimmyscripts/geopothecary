const first_style = [
    //{ featureType: "administrative.country", elementType: "geometry.fill", stylers: [{ }] },
    //{ featureType: "administrative.country", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "administrative.country", elementType: "labels.icon", stylers: [{ }] },
    //{ featureType: "administrative.country", elementType: "labels.text", stylers: [{ }] },
    //{ featureType: "administrative.country", elementType: "labels.text.fill", stylers: [{ }] },
    //{ featureType: "administrative.country", elementType: "labels.text.stroke", stylers: [{ }] },
    { featureType: "administrative.land_parcel", elementType: "geometry.fill", stylers: [{ color: "#C60F20" }] },
    //{ featureType: "administrative.land_parcel", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "administrative.land_parcel", elementType: "labels.icon", stylers: [{ }] },
    //{ featureType: "administrative.land_parcel", elementType: "labels.text", stylers: [{ }] },
    //{ featureType: "administrative.land_parcel", elementType: "labels.text.fill", stylers: [{ }] },
    //{ featureType: "administrative.land_parcel", elementType: "labels.text.stroke", stylers: [{ }] },
    { featureType: "administrative.locality", elementType: "geometry.fill", stylers: [{ color: "#C60F20" }] },
    //{ featureType: "administrative.locality", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "administrative.locality", elementType: "labels.icon", stylers: [{ }] },
    //{ featureType: "administrative.locality", elementType: "labels.text", stylers: [{ }] },
    //{ featureType: "administrative.locality", elementType: "labels.text.fill", stylers: [{ }] },
    //{ featureType: "administrative.locality", elementType: "labels.text.stroke", stylers: [{ }] },
    { featureType: "administrative.neighborhood", elementType: "geometry.fill", stylers: [{ color: "#C60F20" }] },
    //{ featureType: "administrative.neighborhood", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "administrative.neighborhood", elementType: "labels.icon", stylers: [{ }] },
    //{ featureType: "administrative.neighborhood", elementType: "labels.text", stylers: [{ }] },
    //{ featureType: "administrative.neighborhood", elementType: "labels.text.fill", stylers: [{ }] },
    //{ featureType: "administrative.neighborhood", elementType: "labels.text.stroke", stylers: [{ }] },
    { featureType: "administrative.province", elementType: "geometry.fill", stylers: [{ color: "#C60F20" }] },
    //{ featureType: "administrative.province", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "administrative.province", elementType: "labels.icon", stylers: [{ }] },
    //{ featureType: "administrative.province", elementType: "labels.text", stylers: [{ }] },
    //{ featureType: "administrative.province", elementType: "labels.text.fill", stylers: [{ }] },
    //{ featureType: "administrative.province", elementType: "labels.text.stroke", stylers: [{ }] },
    { featureType: "landscape.man_made", elementType: "geometry.fill", stylers: [{ color: "#4DCB4D" }] },
    //{ featureType: "landscape.man_made", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "landscape.man_made", elementType: "labels.icon", stylers: [{ }] },
    //{ featureType: "landscape.man_made", elementType: "labels.text", stylers: [{ }] },
    //{ featureType: "landscape.man_made", elementType: "labels.text.fill", stylers: [{ }] },
    //{ featureType: "landscape.man_made", elementType: "labels.text.stroke", stylers: [{ }] },
    { featureType: "landscape.natural", elementType: "geometry.fill", stylers: [{ color: "#0CA40C" }] },
    //{ featureType: "landscape.natural", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "landscape.natural", elementType: "labels.icon", stylers: [{ }] },
    //{ featureType: "landscape.natural", elementType: "labels.text", stylers: [{ }] },
    //{ featureType: "landscape.natural", elementType: "labels.text.fill", stylers: [{ }] },
    //{ featureType: "landscape.natural", elementType: "labels.text.stroke", stylers: [{ }] },
    { featureType: "landscape.natural.landcover", elementType: "geometry.fill", stylers: [{ color: "#008600" }] },
    //{ featureType: "landscape.natural.landcover", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "landscape.natural.landcover", elementType: "labels.icon", stylers: [{ }] },
    //{ featureType: "landscape.natural.landcover", elementType: "labels.text", stylers: [{ }] },
    //{ featureType: "landscape.natural.landcover", elementType: "labels.text.fill", stylers: [{ }] },
    //{ featureType: "landscape.natural.landcover", elementType: "labels.text.stroke", stylers: [{ }] },
    { featureType: "landscape.natural.terrain", elementType: "geometry.fill", stylers: [{ color: "#006500" }] },
    //{ featureType: "landscape.natural.terrain", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "landscape.natural.terrain", elementType: "labels.icon", stylers: [{ }] },
    //{ featureType: "landscape.natural.terrain", elementType: "labels.text", stylers: [{ }] },
    //{ featureType: "landscape.natural.terrain", elementType: "labels.text.fill", stylers: [{ }] },
    //{ featureType: "landscape.natural.terrain", elementType: "labels.text.stroke", stylers: [{ }] },
    { featureType: "poi.attraction", elementType: "geometry.fill", stylers: [{ color: "#400254" }] },
    //{ featureType: "poi.attraction", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "poi.attraction", elementType: "labels.icon", stylers: [{ }] },
    { featureType: "poi.attraction", elementType: "labels.text", stylers: [{ visibility: "off" }] },
    //{ featureType: "poi.attraction", elementType: "labels.text.fill", stylers: [{ }] },
    { featureType: "poi.attraction", elementType: "labels.text.stroke", stylers: [{ visibility: "off" }] },
    { featureType: "poi.business", elementType: "geometry.fill", stylers: [{ color: "#7F269C" }] },
    //{ featureType: "poi.business", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "poi.business", elementType: "labels.icon", stylers: [{ }] },
    { featureType: "poi.business", elementType: "labels.text", stylers: [{ visibility: "off" }] },
    //{ featureType: "poi.business", elementType: "labels.text.fill", stylers: [{ }] },
    { featureType: "poi.business", elementType: "labels.text.stroke", stylers: [{ visibility: "off" }] },
    //{ featureType: "poi.government", elementType: "geometry.fill", stylers: [{ color: "#a947d7" }] },
    //{ featureType: "poi.government", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "poi.government", elementType: "labels.icon", stylers: [{ }] },
    { featureType: "poi.government", elementType: "labels.text", stylers: [{ visibility: "off" }] },
    //{ featureType: "poi.government", elementType: "labels.text.fill", stylers: [{ }] },
    { featureType: "poi.government", elementType: "labels.text.stroke", stylers: [{ visibility: "off" }] },
    //{ featureType: "poi.medical", elementType: "geometry.fill", stylers: [{ }] },
    //{ featureType: "poi.medical", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "poi.medical", elementType: "labels.icon", stylers: [{ }] },
    { featureType: "poi.medical", elementType: "labels.text", stylers: [{ visibility: "off" }] },
    //{ featureType: "poi.medical", elementType: "labels.text.fill", stylers: [{ }] },
    { featureType: "poi.medical", elementType: "labels.text.stroke", stylers: [{ visibility: "off" }] },
    { featureType: "poi.park", elementType: "geometry.fill", stylers: [{ color: "#6A0F87" }] },
    //{ featureType: "poi.park", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "poi.park", elementType: "labels.icon", stylers: [{ }] },
    { featureType: "poi.park", elementType: "labels.text", stylers: [{ color: "#777777" }] },
    //{ featureType: "poi.park", elementType: "labels.text.fill", stylers: [{ }] },
    { featureType: "poi.park", elementType: "labels.text.stroke", stylers: [{ visibility: "off" }] },
    //{ featureType: "poi.place_of_worship", elementType: "geometry.fill", stylers: [{ }] },
    //{ featureType: "poi.place_of_worship", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "poi.place_of_worship", elementType: "labels.icon", stylers: [{ }] },
    { featureType: "poi.place_of_worship", elementType: "labels.text", stylers: [{ visibility: "off" }] },
    //{ featureType: "poi.place_of_worship", elementType: "labels.text.fill", stylers: [{ }] },
    { featureType: "poi.place_of_worship", elementType: "labels.text.stroke", stylers: [{ visibility: "off" }] },
    { featureType: "poi.school", elementType: "geometry.fill", stylers: [{  color: "#FF4157"  }] },
    //{ featureType: "poi.school", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "poi.school", elementType: "labels.icon", stylers: [{ }] },
    { featureType: "poi.school", elementType: "labels.text", stylers: [{ visibility: "off" }] },
    //{ featureType: "poi.school", elementType: "labels.text.fill", stylers: [{ }] },
    { featureType: "poi.school", elementType: "labels.text.stroke", stylers: [{ visibility: "off" }] },
    //{ featureType: "poi.sports_complex", elementType: "geometry.fill", stylers: [{ }] },
    //{ featureType: "poi.sports_complex", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "poi.sports_complex", elementType: "labels.icon", stylers: [{ }] },
    { featureType: "poi.sports_complex", elementType: "labels.text", stylers: [{ visibility: "off" }] },
    //{ featureType: "poi.sports_complex", elementType: "labels.text.fill", stylers: [{ }] },
    { featureType: "poi.sports_complex", elementType: "labels.text.stroke", stylers: [{ visibility: "off" }] },
    //{ featureType: "road.arterial", elementType: "geometry.fill", stylers: [{ }] },
    //{ featureType: "road.arterial", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "road.arterial", elementType: "labels.icon", stylers: [{ }] },
    { featureType: "road.arterial", elementType: "labels.text", stylers: [{ color: "#777777"}] },
    //{ featureType: "road.arterial", elementType: "labels.text.fill", stylers: [{ }] },
    { featureType: "road.arterial", elementType: "labels.text.stroke", stylers: [{ visibility: "off" }] },
    //{ featureType: "road.highway", elementType: "geometry.fill", stylers: [{ }] },
    //{ featureType: "road.highway", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "road.highway", elementType: "labels.icon", stylers: [{ }] },
    { featureType: "road.highway", elementType: "labels.text", stylers: [{ color: "#777777"}] },
    //{ featureType: "road.highway", elementType: "labels.text.fill", stylers: [{ }] },
    { featureType: "road.highway", elementType: "labels.text.stroke", stylers: [{ visibility: "off" }] },
    //{ featureType: "road.highway.controlled_access", elementType: "geometry.fill", stylers: [{ }] },
    //{ featureType: "road.highway.controlled_access", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "road.highway.controlled_access", elementType: "labels.icon", stylers: [{ }] },
    { featureType: "road.highway.controlled_access", elementType: "labels.text", stylers: [{ color: "#777777"}] },
    //{ featureType: "road.highway.controlled_access", elementType: "labels.text.fill", stylers: [{ }] },
    { featureType: "road.highway.controlled_access", elementType: "labels.text.stroke", stylers: [{ visibility: "off" }] },
    //{ featureType: "road.local", elementType: "geometry.fill", stylers: [{ }] },
    //{ featureType: "road.local", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "road.local", elementType: "labels.icon", stylers: [{ }] },
    { featureType: "road.local", elementType: "labels.text", stylers: [{ color: "#777777"}] },
    //{ featureType: "road.local", elementType: "labels.text.fill", stylers: [{ }] },
    { featureType: "road.local", elementType: "labels.text.stroke", stylers: [{ visibility: "off" }] },
    //{ featureType: "transit.line", elementType: "geometry.fill", stylers: [{ }] },
    //{ featureType: "transit.line", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "transit.line", elementType: "labels.icon", stylers: [{ }] },
    //{ featureType: "transit.line", elementType: "labels.text", stylers: [{ }] },
    //{ featureType: "transit.line", elementType: "labels.text.fill", stylers: [{ }] },
    //{ featureType: "transit.line", elementType: "labels.text.stroke", stylers: [{ }] },
    //{ featureType: "transit.station", elementType: "geometry.fill", stylers: [{ }] },
    //{ featureType: "transit.station", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "transit.station", elementType: "labels.icon", stylers: [{ }] },
    //{ featureType: "transit.station", elementType: "labels.text", stylers: [{ }] },
    //{ featureType: "transit.station", elementType: "labels.text.fill", stylers: [{ }] },
    //{ featureType: "transit.station", elementType: "labels.text.stroke", stylers: [{ }] },
    //{ featureType: "transit.station.airport", elementType: "geometry.fill", stylers: [{ }] },
    //{ featureType: "transit.station.airport", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "transit.station.airport", elementType: "labels.icon", stylers: [{ }] },
    //{ featureType: "transit.station.airport", elementType: "labels.text", stylers: [{ }] },
    //{ featureType: "transit.station.airport", elementType: "labels.text.fill", stylers: [{ }] },
    //{ featureType: "transit.station.airport", elementType: "labels.text.stroke", stylers: [{ }] },
    //{ featureType: "transit.station.bus", elementType: "geometry.fill", stylers: [{ }] },
    //{ featureType: "transit.station.bus", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "transit.station.bus", elementType: "labels.icon", stylers: [{ }] },
    //{ featureType: "transit.station.bus", elementType: "labels.text", stylers: [{ }] },
    //{ featureType: "transit.station.bus", elementType: "labels.text.fill", stylers: [{ }] },
    //{ featureType: "transit.station.bus", elementType: "labels.text.stroke", stylers: [{ }] },
    //{ featureType: "transit.station.rail", elementType: "geometry.fill", stylers: [{ }] },
    //{ featureType: "transit.station.rail", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "transit.station.rail", elementType: "labels.icon", stylers: [{ }] },
    //{ featureType: "transit.station.rail", elementType: "labels.text", stylers: [{ }] },
    //{ featureType: "transit.station.rail", elementType: "labels.text.fill", stylers: [{ }] },
    //{ featureType: "transit.station.rail", elementType: "labels.text.stroke", stylers: [{ }] },
    { featureType: "water", elementType: "geometry.fill", stylers: [{ color: "#5C64FF"}] },
    //{ featureType: "water", elementType: "geometry.stroke", stylers: [{ }] },
    //{ featureType: "water", elementType: "labels.icon", stylers: [{ }] },
    //{ featureType: "water", elementType: "labels.text", stylers: [{ }] },
    //{ featureType: "water", elementType: "labels.text.fill", stylers: [{ }] },
    //{ featureType: "water", elementType: "labels.text.stroke", stylers: [{ }] }
];