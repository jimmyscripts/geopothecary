const getStyle = (style) => {
    let newStyle = [];
    switch(style) {
        case 'first':
            newStyle = first_style;
            break;
        case 'basic':
            newStyle = basic_style;
            break;
        case 'dark':
            newStyle = dark_style;
            break;
        case 'silver':
            newStyle = silver_style;
            break;
        case 'gold':
            newStyle = gold_style;
            break;
        case 'avocado':
            newStyle = avocado_style;
            break;
        case 'brown':
            newStyle = brown_style;
            break;
        default:
            newStyle = [];
    }

    return newStyle;
}