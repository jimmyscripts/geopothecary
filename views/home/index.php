<div id="mapWrapper">
    <div id="map">
    </div>
</div>
<style>
    .mapContainerOverride { margin-left: 0; margin-right: 0; padding: 0; }
    #mapWrapper { position: relative; width: 100vw; height: calc(100vh - 127px); margin: -3rem auto 0rem auto; }
    #map { width: 100%; height: 100%; position: absolute; left: 0; top: 0px; z-index: 1000; }
    .itemMarker { width: 300px; }
</style>
<script src="https://unpkg.com/@googlemaps/markerclusterer/dist/index.min.js"></script>
<script src="views/home/map_styles/first_style.js"></script>
<script src="views/home/map_styles/basic_style.js"></script>
<script src="views/home/map_styles/dark_style.js"></script>
<script src="views/home/map_styles/silver_style.js"></script>
<script src="views/home/map_styles/gold_style.js"></script>
<script src="views/home/map_styles/brown_style.js"></script>
<script src="views/home/map_styles/avocado_style.js"></script>
<script src="views/home/map_styles/style_loader.js" type="text/javascript"></script>
<script>
    const loadedStyle = getStyle('brown');

    async function initMap() {

        let pos = { lat: -34.397, lng: 150.644 };

        let map = new google.maps.Map(document.getElementById("map"), {
            center: pos,
            zoom: 15,
            styles: loadedStyle
        });

        let infoWindow = new google.maps.InfoWindow();

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                async (position) => {
                    const pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };

                    map.setCenter(pos);

                    const mapSources = await getResourcesForMap(pos);
                    let zIndexInc = 4000;
                    const iconSize = 75;

                    let lastLat = 0;
                    let lastLng = 0;
                    let locIteration = 0;
                    let markers = [];

                    mapSources.forEach(locationSources => {
                        console.log(locationSources);
                        const location = locationSources['location'];
                        const source = locationSources['sources'];
                        let locationType = source.length > 0 && location['location_type'] ? location['location_type'] : '1-icon';
                        let highestRarity = 1;
                        source.forEach(item => {
                            if(item['source_rarity'] > highestRarity) {
                                highestRarity = item['source_rarity'];
                            }
                        });

                        let rarityColor = '#191919';
                        switch (highestRarity) {
                            case 1:
                                rarityColor = '#191919';
                                break;
                            case 2:
                                rarityColor = '#633000';
                                break;
                            case 3:
                                rarityColor = '#7f0000';
                                break;
                            case 4:
                                rarityColor = '#7f003f';
                                break;
                            case 5:
                                rarityColor = '#7f007f';
                                break;
                            case 6:
                                rarityColor = '#3f007f';
                                break;
                            case 7:
                                rarityColor = '#00007f';
                                break;
                            case 8:
                                rarityColor = '#cd7f32';
                                break;
                            case 9:
                                rarityColor = '#c0c0c0';
                                break;
                            case 10:
                                rarityColor = '#ffd700';
                                break;
                            default:
                                rarityColor = '#191919';
                        }

                        let contentString = ``;
                        contentString += `<div class="p-3" style="min-width: 220px;">`;
                        if(source.length > 0) { contentString += `<div><h6>${location['location_name'] || location['google_name']} (${location['location_id']})</h6>
                                <p>${location['location_type_name']}</p>
                            </div>`; }
                        source.forEach(item => {
                            contentString += `<span>
                                        <img
                                            style="width: 50px; height: 50px; cursor: pointer;"
                                            title="${item['item_name']}"
                                            src="${window.location.protocol}//${window.location.host}/shared/svg/${item['item_slug']}.svg"
                                            data-bs-toggle="tooltip" data-bs-placement="top"
                                        />
                                    </span>`;
                        });
                        if(location['location_presenter_name']) {
                            contentString += `<div class="mt-2">Presented by:
                                <a class="mapAnchor" href="${location['location_presenter_link']}" target="_blank">
                                    ${location['location_presenter_name']}
                                </a>
                            </div>`;
                        }
                        contentString += `</div>`;

                        if(source.length > 0) {
                            const image = {
                                url: `${window.location.protocol}//${window.location.host}/shared/svg/location_types/${locationType}.svg`,
                                size: new google.maps.Size(iconSize, iconSize),
                                scaledSize: new google.maps.Size(iconSize, iconSize)
                            };

                            let currentLat = location['location_lat'];
                            let currentLng = location['location_long'];

                            if(lastLat == location['location_lat'] && lastLng == location['location_long']) {
                                let latInc = 0;
                                let lngInc = 0;

                                switch (locIteration) {
                                    case 0: latInc = 0; lngInc = 0.0005; break;
                                    case 1: latInc = 0.000167; lngInc = 0.000333; break;
                                    case 2: latInc = 0.000333; lngInc = 0.000167; break;
                                    case 3: latInc = 0.0005; lngInc = 0; break;
                                    case 4: latInc = 0.000333; lngInc = -0.000167; break;
                                    case 5: latInc = 0.000167; lngInc = -0.000333; break;
                                    case 6: latInc = 0; lngInc = -0.0005; break;
                                    case 7: latInc = -0.000167; lngInc = -0.000333; break;
                                    case 8: latInc = -0.000333; lngInc =-0.000167; break;
                                    case 9: latInc = -0.0005; lngInc = 0; break;
                                    case 10: latInc = 0.000333; lngInc = 0.000167; break;
                                    case 11: latInc = 0.000167; lngInc = 0.000333; break;
                                    default: latInc = 0; lngInc = 0;
                                }
                                currentLat = location['location_lat'] + latInc;
                                currentLng = location['location_long'] + lngInc;
                                locIteration++;
                            } else {
                                currentLat = location['location_lat'];
                                currentLng = location['location_long'];
                                lastLat = location['location_lat'];
                                lastLng = location['location_long'];
                                locIteration = 0;
                            }

                            const cPosition = {lat: currentLat, lng: currentLng};

                            const marker = new google.maps.Marker({
                                position: cPosition,
                                map,
                                icon: image,
                                label: {
                                    text: `${highestRarity}`,
                                    fontWeight: 'bold',
                                    fontSize: '16px',
                                    color: `${rarityColor}`,
                                    className: 'locationLabel'
                                },
                                zIndex: zIndexInc
                            });
                            zIndexInc++;

                            marker.addListener("click", () => {
                                infoWindow.setContent(contentString);
                                infoWindow.open(map, marker);
                            });

                            map.addListener('zoom_changed', () => {
                                infoWindow.close();
                            });

                            markers.push(marker);
                        }
                    });
                    new markerClusterer.MarkerClusterer({ map, markers });
                },
                () => {
                    handleLocationError(true, infoWindow, map.getCenter());
                }
            );

        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(
            browserHasGeolocation
                ? "Error: The Geolocation service failed."
                : "Error: Your browser doesn't support geolocation."
        );
        infoWindow.open(map);
    }

    window.initMap = initMap;

    const getResourcesForMap = async (pos) => {

        const data = {
            action:'search-resources-for-map',
            controller:'SearchController',
            output:'JSON',
            data: pos
        }

        const url = `/shared/post.php`;
        const response = await fetch(url, {
            method: 'POST',
            mode: "same-origin",
            credentials: 'same-origin',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify(data)
        });
        if(!checkResponseErrors(response)){
            return;
        }

        const searchResponse = await response.json();
        const resourceList = searchResponse.responseData;
        return resourceList;
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoSwa0cL5DqtoRVUPaaz2ghOJJsW3QMII&callback=initMap"></script>
<style>
    .locationLabel {
        background-color: rgba(256, 256, 256, 1);
        width: 22px;
        height: 22px;
        padding: 1px 1px 1px 0px;
        border-radius: 100%;
        text-align: center;
        text-shadow: 1px 1px gray;
        position: relative;
        top: -35px;
        left: -25px;
    }
    .gm-style .gm-style-iw-c {
        /*background-color: transparent;*/
        padding: 0px;
        border: 4px solid transparent;
        border-image: url("../../shared/svg/stick-border.svg") 30 stretch;
    }
    .gm-style .gm-style-iw-d {
        overflow: hidden !important;
    }
    .mapAnchor {
        text-decoration: none;
        color: darkblue;
        font-weight: bold;
    }
    button.gm-ui-hover-effect {
        top: 7px !important;
        right: 7px !important;
    }
</style>