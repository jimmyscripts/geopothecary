<?php

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

/** GEOPOTHECARY DB CREDENTIALS */
define('GEO_DB_HOST', $_ENV['GEO_DB_HOST']);

/** Name of geopothecary database */
define('GEO_DB_NAME', $_ENV['GEO_DB_NAME']);

/** MySQL database password */
define('GEO_DB_PORT', $_ENV['GEO_DB_PORT']);

/** MySQL database username */
define('GEO_DB_USER', $_ENV['GEO_DB_USER']);

/** MySQL database password */
define('GEO_DB_PASSWORD', $_ENV['GEO_DB_PASSWORD']);

