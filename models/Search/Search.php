<?php
class Search {
    /**
     * Set the pickup data array.
     *
     * @param int $locationId
     * @param int $resourceId
     * @return array <Search>
     */
    public function setPickupData(int $locationId, int $resourceId):array {
        return $this->savePickupData($locationId, $resourceId);
    }

    /**
     * Get the search data array.
     *
     * @param int $locationId
     * @param int $sourceId
     * @return array <Search>
     */
    private function savePickupData(int $locationId, int $sourceId):array {

        $user = new User();
        $userData = $user->setUser();
        $userId = $userData->user_id;
        $personal = 'personal';
        $personalSlots = 360;
        $hotBar = 'hotBar';
        $hotBarSlots = 6;

        //Find which resources inventory slot is available
        $sql = 'SELECT * FROM `tbl_resources` WHERE `user_id` = :user_id AND (`inventory_id` = :hotBar OR `inventory_id` = :personal)';

        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute(['user_id' => $userId, 'hotBar' => $hotBar, 'personal' => $personal]);
            $result = $stmt->fetchAll();
            $slots = $result ? $result : [];
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not get slot data.', 0, $e, 3);
        }

        $firstEmptyInventory = '';
        $firstEmptySlot = '';
        for($i = 0; $i < $hotBarSlots; $i++) {
            $slotCheck = array_filter($slots, function($slot) use ($i) {
                return $slot['resource_inventory_position'] == $i && $slot['inventory_id'] == 'hotBar';
            });
            if(empty($slotCheck)) {
                $firstEmptySlot = $i;
                $i = $hotBarSlots;
                $firstEmptyInventory = 'hotBar';
            } else {
                for($r = 0; $r < $personalSlots; $r++) {
                    $slotCheck = array_filter($slots, function($slot) use ($r) {
                        return $slot['resource_inventory_position'] == $r && $slot['inventory_id'] == 'personal';
                    });
                    if(empty($slotCheck)) {
                        $firstEmptySlot = $r;
                        $r = $personalSlots;
                        $firstEmptyInventory = 'personal';
                    } else {

                    }
                }
            }
        }

        if(!$firstEmptyInventory && !$firstEmptySlot) {
            throw new GEORequestException('Inventory is full!');
        }

        //Get resource data from source id
        $sql = 'SELECT * FROM `tbl_sources` `ts` JOIN `lkp_items` `li` ON `li`.`item_id` = `ts`.`item_id` WHERE `ts`.`source_id` = :sourceId';

        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute(['sourceId' => $sourceId]);
            $result = $stmt->fetch();
            $source = $result ? $result : [];
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not get source item data.', 0, $e, 3);
        }
        $source['inventory_id'] = $firstEmptyInventory;
        $source['resource_inventory_position'] = $firstEmptySlot;

        //Save to our resources inventory
        $sql = 'INSERT INTO `tbl_resources` 
            (`source_id`, `item_id`, `resource_count`, `resource_lore`, `user_id`, `inventory_id`, `resource_inventory_position`) 
            VALUES (:source_id, :item_id, :source_count, :source_lore, :user_id, :inventory_id, :slot)';

        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute(['source_id' => $source['source_id'],
                'item_id' => $source['item_id'],
                'source_count' => $source['source_count'],
                'source_lore' => $source['source_lore'],
                'user_id' => $userId,
                'inventory_id' => $source['inventory_id'],
                'slot' => $source['resource_inventory_position']]);
            $resource_id = $conn->lastInsertId();
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not get search data.', 0, $e, 3);
        }
        $source['resource_id'] = $resource_id;

        //Save to our visit
        $sql = 'INSERT INTO `tbl_visits` (`user_id`, `location_id`, `source_id`) VALUES (:userId, :locationId, :sourceId)';

        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute(['userId' => $userId, 'locationId' => $locationId, 'sourceId' => $sourceId]);
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not get search data.', 0, $e, 3);
        }

        return $source;
    }
}
?>