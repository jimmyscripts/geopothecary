<?php

class SearchFactory {
    /**
     * Set the search data array.
     *
     * @param array $values
     * @return array <Search>
     * @throws GEOServerException
     */
    public function setSourceData(array $values):array {
        return $this->getSourceData($values);
    }

    /**
     * Set the search data array.
     *
     * @param float $lat
     * @param float $long
     * @return array <Search>
     * @throws GEOServerException
     */
    public function setMapData(float $lat, float $long):array {
        return $this->getMapData($lat, $long);
    }

    /**
     * Get the search data array.
     *
     * @param array $values
     * @return array <Search>
     * @throws GEOServerException
     */
    private function getSourceData(array $values):array {
        $lat = $values['data']['lat'];
        $long = $values['data']['long'];

        $searchData = [];

        $searchRange = 0.5;
        $location = new Location();
        $locations = $location->getLocationsByPositRange($lat, $long, $searchRange);

        $collectableRange = 0.1;

        //Then we check locations already collected from in range locations today.
        $collected = [];
        foreach($locations as $key => $location) {
            $location_id = $location['location_id'];
            $sql = 'SELECT `location_id` FROM `tbl_visits` 
                WHERE `location_id` = ? 
                AND `user_id` = ?
                AND `visit_date` = (SELECT MAX(`visit_date`) FROM `tbl_visits` WHERE `location_id` = ?)
                AND `visit_date` >= now() - INTERVAL 1 DAY';

            try {
                $conn = Database::connectMain();
                $stmt = $conn->prepare($sql);
                $stmt->execute([$location_id, $values['user']->user_id, $location_id]);
                $collected = $stmt->fetchAll();

                if($collected) {unset($locations[$key]);}

                $distanceToLocation = round(sqrt((abs($location['location_lat'] - $lat)) + (abs($location['location_long'] - $long))), 3);
                if($distanceToLocation > $collectableRange) {
                    $location['distanceToLocation'] = $distanceToLocation;
                    $searchData['nearby'][] = $location;
                    unset($locations[$key]);
                }
            } catch (\PDOException $e) {
                throw new GEOServerException('Could not get visit data.', 0, $e, 3);
            }
        }
        $searchData['collected'] = $collected;
        $searchData['locations'] = $locations;

        $search = [];
        foreach($locations as $key => $location) {
            $location_id = $location['location_id'];
            $sql = 'SELECT * FROM `tbl_sources` `ts`
                    LEFT JOIN `lkp_items` `li` ON `li`.`item_id` = `ts`.`item_id`
                    LEFT JOIN `tbl_locations` `tl` ON `ts`.`location_id` = `tl`.`location_id`
                    WHERE (`tl`.`location_id` = :location_id)';

            try {
                $conn = Database::connectMain();
                $stmt = $conn->prepare($sql);
                $stmt->execute(['location_id' => $location_id]);
                $result = $stmt->fetchAll();
                $search[] = $result ? $result : [];
            } catch (\PDOException $e) {
                throw new GEOServerException('Could not get search data.', 0, $e, 3);
            }
        }
        $searchData['sources'] = $search;
        return $searchData;
    }

    /**
     * Get the map data array.
     *
     * @param float $lat
     * @param float $long
     * @return array <Search>
     * @throws GEOServerException
     */
    private function getMapData(float $lat, float $long):array {

        $search = [];
        $mapData = [];
        $searchRange = 5;
        $location = new Location();

        $locations = $location->getLocationsByPositRange($lat, $long, $searchRange);

        foreach($locations as $key => $location) {
            $location_id = $location['location_id'];
            $sql = 'SELECT * FROM `tbl_sources` `ts`
                    LEFT JOIN `lkp_items` `li` ON `li`.`item_id` = `ts`.`item_id`
                    WHERE (`ts`.`location_id` = :location_id)';

            try {
                $conn = Database::connectMain();
                $stmt = $conn->prepare($sql);
                $stmt->execute(['location_id' => $location_id]);
                $result = $stmt->fetchAll();
                $search = $result ? $result : [];
            } catch (\PDOException $e) {
                throw new GEOServerException('Could not get search data.', 0, $e, 3);
            }
            $mapData[$key]['location'] = $location;
            $mapData[$key]['sources'] = $search;
        }

        return $mapData;
    }
}