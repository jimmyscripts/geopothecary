<?php

class GEOWarning {
    protected $msg;
    protected $severity;
    protected $category;

    public function __construct(string $msg, int $severity, int $category) {
        $this->msg = $msg;
        $this->severity = $severity;
        $this->category = $category;
    }

    public function getMessage() {
        return $this->msg;
    }

    public function log() {
        $conn = Database::connectMain();
        try {
            Database::insertData($conn, 'geo_log', [
                'log_text' => $this->msg,
                'status' => 1,
                'severity' => $this->severity,
                'category' => $this->category
            ]);
        } catch (\PDOException $e) {
            throw new GEOServerException($e->getMessage(), null, $e, 3);
        }
    }
}
