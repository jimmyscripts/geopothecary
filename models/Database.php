<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/composer/vendor/autoload.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/config.php');

class Database {
    protected static $conn = null;
    protected static $connDb = null;

    public static function connectMain() {
        return Database::connect(GEO_DB_NAME, GEO_DB_USER, GEO_DB_PASSWORD, GEO_DB_HOST);
    }

    public static function connect($db, $user, $pass, $host) {
        if(self::$conn == null || (is_object(self::$conn) && get_class(self::$conn) !== 'PDO') || self::$connDb != $db) {
            self::initConn($db, $user, $pass, $host);
        }
        return self::$conn;
    }

    public static function initConn(string $db, string $user, string $pass, string $host) {
        $charset = "utf8mb4";
        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";

        $defaultParamsArr = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false
        ];

        try {
            self::$conn = new PDO($dsn, $user, $pass, $defaultParamsArr);
            self::$connDb = $db;
        } catch (\PDOException $e) {
//            print_r($e);
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
    }
}