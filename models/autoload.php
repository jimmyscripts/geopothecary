<?php

function autoloadGeoModels($className) {

    $classesDir = $_SERVER['DOCUMENT_ROOT'] . '/models';
    $filename = $className . '.php';

    if(is_readable($classesDir.'/'.$filename)) {
        require_once $classesDir.'/'.$filename;
    }
    if(is_readable($classesDir.'/Auth/'.$filename)) {
        require_once $classesDir.'/Auth/'.$filename;
    }
    if(is_readable($classesDir."/Database/".$filename)) {
        require_once $classesDir."/Database/".$filename;
    }
    if(is_readable($classesDir."/Inventory/".$filename)) {
        require_once $classesDir."/Inventory/".$filename;
    }
    if(is_readable($classesDir."/Search/".$filename)) {
        require_once $classesDir."/Search/".$filename;
    }
    if(is_readable($classesDir."/GEO/".$filename)) {
        require_once $classesDir."/GEO/".$filename;
    }
    if(is_readable($classesDir."/Location/".$filename)) {
        require_once $classesDir."/Location/".$filename;
    }
    if(is_readable($classesDir."/Tool/".$filename)) {
        require_once $classesDir."/Tool/".$filename;
    }
    if(is_readable($classesDir."/Alchemy/".$filename)) {
        require_once $classesDir."/Alchemy/".$filename;
    }
    if(is_readable($classesDir."/Utility/".$filename)) {
        require_once $classesDir."/Utility/".$filename;
    }
    if(is_readable($classesDir."/User/".$filename)) {
        require_once $classesDir."/User/".$filename;
    }
    if(is_readable($classesDir."/SourceBuilder/".$filename)) {
        require_once $classesDir."/SourceBuilder/".$filename;
    }
}
spl_autoload_register('autoloadGeoModels');

?>