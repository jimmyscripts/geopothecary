<?php

class Tool {

    public function setToolData(string $tool):array {
        return $this->getToolData($tool);
    }

    private function getToolData($tool) {

        $user = new User();
        $userData = $user->setUser();
        $userId = $userData->user_id;

        $sql = 'SELECT *, CONCAT(`tr`.`inventory_id`, "_", `tr`.`resource_inventory_position`) as "absolute_inventory_position" FROM `tbl_resources` `tr` 
            INNER JOIN `lkp_items` `li` ON `tr`.`item_id` = `li`.`item_id`
            WHERE `tr`.`user_id` = :user_id AND `tr`.`inventory_id` = :tool';

        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute(['user_id' => $userId, 'tool' => $tool]);
            $toolData = $stmt->fetchAll();

        } catch (\PDOException $e) {
            throw new GEOServerException('Could not get visit data.', 0, $e, 3);
        }

        return $toolData;
    }
}