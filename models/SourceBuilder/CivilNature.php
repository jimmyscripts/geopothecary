<?php

class CivilNature {

    public int $locationId;
    public int $itemId;
    public int $sourceCount;
    public array $sourceLore;
    public int $sourceRarity;

    /**
     * @return array[]
     */
    public function sourceItems():array {
        return [
            ['item_id' => 1010, 'source_count' => 12, 'source_lore' => [], 'source_rarity' => 5],
            ['item_id' => 1008, 'source_count' => 13, 'source_lore' => [], 'source_rarity' => 6],
            ['item_id' => 1002, 'source_count' => 22, 'source_lore' => [], 'source_rarity' => 7]
        ];
    }
}