<?php

class Playground {

    public int $locationId;
    public int $itemId;
    public int $sourceCount;
    public array $sourceLore;
    public int $sourceRarity;

    /**
     * @return array[]
     */
    public function sourceItems():array {
        return [
            ['item_id' => 1000, 'source_count' => 1, 'source_lore' => [], 'source_rarity' => 3],
            ['item_id' => 1004, 'source_count' => 3, 'source_lore' => [], 'source_rarity' => 8],
            ['item_id' => 1005, 'source_count' => 100, 'source_lore' => [], 'source_rarity' => 9]
        ];
    }
}