<?php
class User {

    public function setUser() {
        return $this->getUser();
    }

    private function getUser() {
        $this->username = $_SESSION['username'];
        $this->user_id = $_SESSION['user_id'];
        $this->user_role = $_SESSION['user_role'];
        return $this;
    }
    // (C) GET USER
    private function get($id, $idType) {
        $sql = sprintf('SELECT * FROM `users` WHERE `user_%s`=?', $idType == 'email' ? 'email' : 'name');
        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute([$id]);
            $result = $stmt->fetch();
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not verify id or email.', 0, $e, 3);
        }
        return $result;
    }

    public function loginUser($login, $pass) {
        $user = $this->get($login, 'name');

        if(empty($user)) {
            $user = $this->get($login, 'email');
            if(empty($user)) {
                throw new GEORequestException('This user does not exist.');
            }
        }

        if(password_verify($pass, $user['user_password'])) {
            if($user['user_activate']) {
                //throw new GEORequestException('Please verify your email.');
            }
            session_start();
            $_SESSION['username'] = $user['user_name'];
            $_SESSION['user_id'] = $user['user_id'];
            $_SESSION['user_role'] = $user['user_role'];
        } else {
            throw new GEORequestException('Incorrect password.');
        }
    }

    public function logoutUser() {
        session_start();
        // Unset all of the session variables.
        $_SESSION = array();

        // Destroy the session.
        session_destroy();
    }

    // (D) REGISTER NEW USER
    public function registerUser($email, $pass, $cpass, $username):bool {
        // (D1) CHECK IF USER REGISTERED
        // @TODO - WHAT TO DO IF THERE IS AN EXISTING ACTIVATION?
        // ALLOW USER TO GENERATE NEW TOKEN & EMAIL AFTER SOME TIME?
        // MANUAL CONTACT ADMIN?
        $checkEmail = $this->get($email, 'email');
        if(!empty($checkEmail)) {
            throw new GEORequestException('This email is already in use.');
        }

        $checkUsername = $this->get($username, 'name');
        if(!empty($checkUsername)) {
            throw new GEORequestException('This username is already in use.');
        }

        // (D2) CHECK PASSWORD
        if($pass != $cpass) {
            throw new GEORequestException('Passwords do not match.');
        }

        if(!$username) {
            throw new GEORequestException('Please create a username.');
        }

        // (D3) GENERATE NOT-SO-RANDOM TOKEN HASH FOR VERIFICATION
        $token = md5(date("YmdHis") . $this->email);

        // (D4) INSERT INTO DATABASE
        $sql = 'INSERT INTO `users` (`user_email`, `user_password`, `user_activate`, `user_name`) VALUES (?,?,?,?)';
        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute([$email, password_hash($pass, PASSWORD_DEFAULT), $token, $username]);
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not save new user.', 0, $e, 3);
        }

        // (D5) SEND CONFIRMATION EMAIL
        // @TODO - CHANGE TO YOUR OWN MESSAGE + URL!
        $url = "http://localhost/confirm.php";
        $msg = sprintf(
            "Visit this <a href='%s?i=%u&h=%s'>link</a> to complete your registration.",
            $url, $this->lastID, $token
        );
        if (@mail(
            $email, "Confirm your email", $msg,
            implode("\r\n", ["MIME-Version: 1.0", "Content-type: text/html; charset=utf-8"])
        )) { return true; } else {
            $this->error = "Error sending out email";
            return false;
        }
    }

    // (E) VERIFY REGISTRATION
    function verify($id, $hash) {
        // (E1) GET + CHECK THE USER
        $user = $this->get($id, 'email');
        if ($user === false) {
            $this->error = "User not found.";
            return false;
        }
        if (!isset($user["user_activate"])) {
            $this->error = "Account already activated.";
            return false;
        }
        if ($user["user_activate"] != $hash) {
            $this->error = "Invalid token.";
            return false;
        }

        // (E2) ACTIVATE ACCOUNT IF OK
        try {
            $this->stmt = $this->pdo->prepare("UPDATE `users` SET `user_activate`=NULL WHERE `user_id`=?");
            $this->stmt->execute([$id]);
            $this->lastID = $this->pdo->lastInsertId();
        } catch (Exception $ex) {
            $this->error = $ex;
            return false;
        }

        // @TODO - (E3) SEND WELCOME MESSAGE IF YOU WANT
        // mail ($user["user_email"], "WELCOME!", "Welcome message here.");
        return true;
    }
}

// (G) NEW USER OBJECT
$USR = new User();
