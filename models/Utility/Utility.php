<?php

class Utility {
    public function getLocations() {
        return $this->setLocations();
    }
    public function getSources() {
        return $this->setSources();
    }
    public function getItems() {
        return $this->setItems();
    }

    /**
     *Save/Update locations
     * @param float $location_lat
     * @param float $location_long
     * @param string $google_name
     * @param string $google_address
     * @param string $google_types
     * @param string $google_queries
     */
    public function savePlace(
        float $latitude,
        float $longitude,
        string $google_name,
        string $google_address,
        string $google_types,
        string $google_queries
    ):void {
        $google_queries = json_encode(explode(' ', $google_queries));
        $sql = 'INSERT INTO `tbl_places` (`latitude`, `longitude`, `google_name`, `google_address`, `google_types`, `google_queries`) 
                VALUES (:latitude, :longitude, :google_name, :google_address, :google_types, :google_queries)';

        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute([
                'latitude' => $latitude,
                'longitude' => $longitude,
                'google_name' => $google_name,
                'google_address' => $google_address,
                'google_types' => $google_types,
                'google_queries' => $google_queries
            ]);
        } catch (\PDOException $e) {
            if ($e->errorInfo[1] == 1062) {
                //The INSERT query failed due to a key constraint violation. Let's try and update
                //But first we need to get the google queries.
                $sql = 'SELECT `google_queries` FROM `tbl_places` WHERE `google_address` = :google_address';
                try{
                    $conn = Database::connectMain();
                    $stmt = $conn->prepare($sql);
                    $stmt->execute([
                        'google_address' => $google_address
                    ]);
                    $googleQueries = $stmt->fetch();
                } catch (\PDOException $e) {
                    throw new GEOServerException('Could not get google_queries.', 0, $e, 3);
                }

                $googleQueries = json_decode($googleQueries['google_queries']);
                $google_queries = json_decode($google_queries);

                if(isset($googleQueries)) {
                    foreach($googleQueries as $googleQuery) {
                        if(!in_array($googleQuery, $google_queries)) {
                            $google_queries[] = $googleQuery;
                        }
                    }
                }

                $google_queries = json_encode($google_queries);

                $sql = 'UPDATE `tbl_places` SET 
                           `latitude` = :latitude, 
                           `longitude` = :longitude,  
                           `google_name` = :google_name,
                           `google_types` = :google_types,
                           `google_queries` = :google_queries
                           WHERE `google_address` = :google_address';
                try{
                    $conn = Database::connectMain();
                    $stmt = $conn->prepare($sql);
                    $stmt->execute([
                        'latitude' => $latitude,
                        'longitude' => $longitude,
                        'google_name' => $google_name,
                        'google_types' => $google_types,
                        'google_queries' => $google_queries,
                        'google_address' => $google_address
                    ]);
                } catch (\PDOException $e) {
                    throw new GEOServerException('Could not update location data.', 0, $e, 3);
                }
            } else {
                throw new GEOServerException('Could not save location data.', 0, $e, 3);
            }
        }
    }

    /**
     *
     */
    private function setLocations() {
        $user = new User();
        $userData = $user->setUser();
        $userRole = $userData->user_role;

        if($userRole != 'admin') {
            throw new GEORequestException('Must be admin to fetch all locations data');
        }

        $sql = 'SELECT * FROM `tbl_locations`';
        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $locations = $stmt->fetchAll();
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not get location data.', 0, $e, 3);
        }
        return $locations;
    }

    /**
     *
     */
    private function setSources() {
        $user = new User();
        $userData = $user->setUser();
        $userRole = $userData->user_role;

        if($userRole != 'admin') {
            throw new GEORequestException('Must be admin to fetch all locations data');
        }

        $sql = 'SELECT * FROM `tbl_sources`';
        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $sources = $stmt->fetchAll();
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not get location data.', 0, $e, 3);
        }
        return $sources;
    }

    /**
     *
     */
    private function setItems() {
        $user = new User();
        $userData = $user->setUser();
        $userRole = $userData->user_role;

        if($userRole != 'admin') {
            throw new GEORequestException('Must be admin to fetch all locations data');
        }

        $sql = 'SELECT * FROM `lkp_items`';
        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $items = $stmt->fetchAll();
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not get location data.', 0, $e, 3);
        }

        $itemList = [];
        foreach($items as $item) {
            $itemList[$item['item_id']] = $item;
        }

        return $itemList;
    }

    /**
     * Add new resource
     * @param int $locationId
     * @param int $itemSelect
     * @param int $itemCount
     * @param int $rarity
     * @throws GEORequestException|GEOServerException
     */
    public function addNewResource(int $locationId, int $itemSelect, int $itemCount, int $rarity) {

        $sql = 'INSERT INTO `tbl_sources` (`location_id`, `item_id`, `source_count`, `source_rarity`) VALUES (:location_id, :item_id, :source_count, :source_rarity)';
        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute([
                'location_id' => $locationId,
                'item_id' => $itemSelect,
                'source_count' => $itemCount,
                'source_rarity' => $rarity
            ]);
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not add new resources.', 0, $e, 3);
        }
    }

    /**
     * Add new location
     * @param string $location_lat
     * @param string $location_long
     * @param string $location_name
     * @param string $location_address
     * @throws GEORequestException|GEOServerException
     */
    public function addNewLocation(string $location_lat, string $location_long, string $location_name, string $location_address) {

        $sql = 'INSERT INTO `tbl_locations` (`location_lat`, `location_long`, `location_name`, `location_address`) VALUES (:location_lat, :location_long, :location_name, :location_address)';
        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute([
                'location_lat' => $location_lat,
                'location_long' => $location_long,
                'location_name' => $location_name,
                'location_address' => $location_address
            ]);
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not add new resources.', 0, $e, 3);
        }
    }

    /**
     * Run default item builder
     * @return void
     */
    public function runDefaultListBuilder():void {
        $sql = 'SELECT `place_id`, `google_name`, `google_types`, `google_queries` FROM `tbl_places` ';
        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $queries = $stmt->fetchAll();
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not fetch location queries.', 0, $e, 3);
        }

        foreach($queries as $query) {
            if($query['google_types'] && $query['google_queries']) {
                $placeId = $query['place_id'];
                $types = json_decode($query['google_types']);
                $queryList = json_decode($query['google_queries']);

                $defaultList = ['general'];

                $civilNatureTypeList = ['park', 'zoo', 'amusement_park', 'tourist_attraction', 'rv_park'];
                $civilAquaticTypeList = ['park', 'tourist_attraction'];
                $retailIndoorTypeList = ['store', 'tourist_attraction'];
                $retailOutdoorTypeList = ['store'];
                $deepNatureTypeList = ['natural_feature'];
                $deepAquaticTypeList = ['natural_feature'];
                $educationTypeList = ['school', 'university', 'zoo', 'aquarium', 'library'];

                $civilNatureQueryList = ['playground', 'park', 'field', 'garden', 'herbs', 'farm', 'zoo', 'aquarium', 'butterfly'];
                $civilAquaticQueryList = ['creek', 'river', 'marina', 'lake', 'dock', 'fishing', 'boat', 'bridge', 'ferry', 'aquarium', 'pond'];
                $retailIndoorQueryList = ['herb', 'vegetable', 'bee', 'honey', 'egg', 'chicken', 'natural', 'flea', 'pet', 'aquarium', 'grocery', 'haunted'];
                $retailOutdoorQueryList = ['farm', 'garden', 'zoo'];
                $deepNatureQueryList = ['camping', 'camp', 'preserve', 'reserve'];
                $deepAquaticQueryList = ['lake', 'ocean', 'river'];
                $educationQueryList = ['school', 'college', 'university', 'library', 'museum', 'historic', 'books', 'campus'];

                $specificCivilNatureQueryList = ['farm', 'garden', 'zoo', 'aquarium', 'playground'];
                $specificCivilAquaticQueryList = ['bridge', 'ferry', 'marina', 'fishing'];
                $specificRetailIndoorQueryList = ['grocery', 'restaurant'];
                $specificRetailOutdoorQueryList = ['market'];
                $specificDeepNatureQueryList = ['trail', 'camp', 'reserve', 'preserve'];
                $specificDeepAquaticQueryList = ['pond', 'lake', 'river', 'trail', 'creek'];
                $specificEducationQueryList = ['school', 'university', 'library', 'historic'];

                if($this->filterDefaults($types, $queryList, $civilNatureTypeList, $civilNatureQueryList)) {
                    $defaultList[] = 'civil nature';
                    $specificIntersect = array_intersect($queryList, $specificCivilNatureQueryList);
                    foreach($specificIntersect as $intersect) {
                        $defaultList[] = $intersect;
                    }
                }
                if($this->filterDefaults($types, $queryList, $civilAquaticTypeList, $civilAquaticQueryList)) {
                    $defaultList[] = 'civil aquatic';
                    $specificIntersect = array_intersect($queryList, $specificCivilAquaticQueryList);
                    foreach($specificIntersect as $intersect) {
                        $defaultList[] = $intersect;
                    }
                }
                if($this->filterDefaults($types, $queryList, $retailIndoorTypeList, $retailIndoorQueryList)) {
                    $defaultList[] = 'retail indoor';
                    $specificIntersect = array_intersect($queryList, $specificRetailIndoorQueryList);
                    foreach($specificIntersect as $intersect) {
                        $defaultList[] = $intersect;
                    }
                }
                if($this->filterDefaults($types, $queryList, $retailOutdoorTypeList, $retailOutdoorQueryList)) {
                    $defaultList[] = 'retail outdoor';
                    $specificIntersect = array_intersect($queryList, $specificRetailOutdoorQueryList);
                    foreach($specificIntersect as $intersect) {
                        $defaultList[] = $intersect;
                    }
                }
                if($this->filterDefaults($types, $queryList, $deepNatureTypeList, $deepNatureQueryList)) {
                    $defaultList[] = 'deep nature';
                    $specificIntersect = array_intersect($queryList, $specificDeepNatureQueryList);
                    foreach($specificIntersect as $intersect) {
                        $defaultList[] = $intersect;
                    }
                }
                if($this->filterDefaults($types, $queryList, $deepAquaticTypeList, $deepAquaticQueryList)) {
                    $defaultList[] = 'deep aquatic';
                    $specificIntersect = array_intersect($queryList, $specificDeepAquaticQueryList);
                    foreach($specificIntersect as $intersect) {
                        $defaultList[] = $intersect;
                    }
                }
                if($this->filterDefaults($types, $queryList, $educationTypeList, $educationQueryList)) {
                    $defaultList[] = 'education';
                    $specificIntersect = array_intersect($queryList, $specificEducationQueryList);
                    foreach($specificIntersect as $intersect) {
                        $defaultList[] = $intersect;
                    }
                }

                $defaultList = json_encode($defaultList);

                $sql = 'UPDATE `tbl_places` SET `default_list` = :default_list WHERE `place_id` = :place_id';
                try {
                    $conn = Database::connectMain();
                    $stmt = $conn->prepare($sql);
                    $stmt->execute([
                        'default_list' => $defaultList,
                        'place_id' => $placeId
                    ]);
                } catch (\PDOException $e) {
                    throw new GEOServerException('Could not add default list to location.', 0, $e, 3);
                }
            }
        }
    }

    /**
     *
     */
    private function filterDefaults($types, $qs, $typeList, $q):bool {
        return array_intersect($types, $typeList) && array_intersect($qs, $q);
    }

    /**
     *
     */
    public function runDefaultLocationBuilder():void {
        $sql = 'SELECT * FROM `tbl_places`';
        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $defaultList = $stmt->fetchAll();
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not fetch location queries.', 0, $e, 3);
        }

        foreach($defaultList as $placeDefaults) {
            $createdKey = 'PlaceID:' . $placeDefaults['place_id'];
            $address = $placeDefaults['google_address'];
            $latitude = $placeDefaults['latitude'];
            $longitude = $placeDefaults['longitude'];
            $googleName = $placeDefaults['google_name'];
            $placeDefaultNames = json_decode($placeDefaults['default_list']);

            if($placeDefaults) {
                foreach($placeDefaultNames as $defaultName) {
                    $locationType = str_replace(" ", "_", $defaultName);
                    $sql = 'INSERT INTO `tbl_locations` 
                        (`location_address`, `default_name`, `created_key`, `location_lat`, `location_long`, `location_name`, `location_type`) 
                        VALUES (:address, :defaultName, :createdKey, :latitude, :longitude, :googleName, :locationType)';
                    try {
                        $conn = Database::connectMain();
                        $stmt = $conn->prepare($sql);
                        $stmt->execute([
                            'address' => $address,
                            'defaultName' => $defaultName,
                            'createdKey' => $createdKey,
                            'latitude' => $latitude,
                            'longitude' => $longitude,
                            'googleName' => $googleName,
                            'locationType' => $locationType
                        ]);
                    } catch (\PDOException $e) {
                        $sql = 'UPDATE `tbl_locations` SET `location_lat` = :latitude, `location_long` = :longitude, `location_name` = :googleName, `location_type` = :locationType
                            WHERE `location_address` = :address AND `default_name` = :defaultName AND `created_key` = :createdKey';
                        try {
                            $conn = Database::connectMain();
                            $stmt = $conn->prepare($sql);
                            $stmt->execute([
                                'latitude' => $latitude,
                                'longitude' => $longitude,
                                'googleName' => $googleName,
                                'locationType' => $locationType,
                                'address' => $address,
                                'defaultName' => $defaultName,
                                'createdKey' => $createdKey
                            ]);
                        } catch (\PDOException $e) {
                            throw new GEOServerException('Could not add default locations.', 0, $e, 3);
                        }
                    }
                }
            }
        }
    }

    /**
     * Run Default Source Builder
     * @return void
     */
    public function runDefaultSourceBuilder():void {

        $sql = 'SELECT * FROM `tbl_locations`';
        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $locationList = $stmt->fetchAll();
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not fetch location queries.', 0, $e, 3);
        }

        foreach($locationList as $location) {
            $newSourceArray = [];
            if(!$location['default_name']) {
                continue;
            }

            $className = $this->buildClassName($location['default_name']);

            if(file_exists($className . '.php')) {
                $newSourceArray = $this->buildSources($className, $location['location_id']);
            }

            foreach($newSourceArray as $sourceObject) {
                $sql = 'INSERT INTO `tbl_sources` (`location_id`, `item_id`, `source_count`, `source_lore`, `source_rarity`) 
                    VALUES (:locationId, :itemId, :sourceCount, :sourceLore, :sourceRarity)';
                try {
                    $conn = Database::connectMain();
                    $stmt = $conn->prepare($sql);
                    $stmt->execute([
                        'locationId' => $sourceObject->locationId,
                        'itemId' => $sourceObject->itemId,
                        'sourceCount' => $sourceObject->sourceCount,
                        'sourceLore' => json_encode($sourceObject->sourceLore),
                        'sourceRarity' => $sourceObject->sourceRarity
                    ]);
                } catch (\PDOException $e) {
                    continue;
                }
            }
        }
    }

    /**
     * Builds an array of source objects.
     * @param string $className
     * @param int $locationId
     * @return array
     */
    private function buildSources(string $className, int $locationId):array {
        $sourceArray = [];
        foreach($className->sourceItems() as $key => $sourceItem) {
            $defaultName = new $className;
            $defaultName->locationId = $locationId;
            $defaultName->itemId = $sourceItem['item_id'];
            $defaultName->sourceCount = $sourceItem['source_count'];
            $defaultName->sourceLore = $sourceItem['source_lore'];
            $defaultName->sourceRarity = $sourceItem['source_rarity'];
            $sourceArray[$key] = $defaultName;
        }

        return $sourceArray;
    }

    /**
     * Build class name from default name
     * @param string $defaultName
     * @return object $classname
     */
    private function buildClassName(string $defaultName):object {
        $className = ucwords($defaultName);
        $className = str_replace(" " , "", $className);
        return new $className;
    }
}

