<?php

class InventoryFactory {
    /**
     * Create an inventory object.
     *
     * @param int $userId
     * @param string $invType
     * @return object <Inventory>
     */
    public static function createInventory(int $userId, string $invType):object {
        $inventory = new Inventory();
        $setInventory = $inventory->setInventoryData($userId, $invType);

        return $setInventory;
    }
}