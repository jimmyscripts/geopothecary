<?php

class Inventory {
    /**
     * Set the inventory data array.
     *
     * @param string $invType
     * @return array <Inventory>
     * @throws GEOServerException
     */
    public function setInventoryData(string $invType):array {
        return $this->getInventoryData($invType);
    }

    /**
     * Save new resource counts.
     *
     * @param int $totalCount
     * @param int $targetResourceId
     * @param int $droppedResourceId
     * @return null
     * @throws GEOServerException
     */
    public function saveResourceCount(int $totalCount, int $targetResourceId, int $droppedResourceId) {
        if($this->sendResourceCount($totalCount, $targetResourceId)) {
            $this->removeOldStack($droppedResourceId);
        }
    }

    /**
     * Save new resource position.
     *
     * @param string $inventoryId
     * @param int $positionId
     * @param int $droppedResourceId
     * @return null
     * @throws GEOServerException
     */
    public function saveResourcePosition(string $inventoryId, int $positionId, int $droppedResourceId) {
        $this->sendResourcePosition($inventoryId, $positionId, $droppedResourceId);
    }

    /**
     * Save new resource
     *
     * @param string $inventoryId
     * @param int $positionId
     * @param int $newItemId
     * @param int $newResourceCount
     * @param string $newResourceLore
     * @return int
     * @throws GEOServerException
     */
    public function saveNewResource(string $inventoryId, int $positionId, int $newItemId, int $newResourceCount, string $newResourceLore):int {
        return $this->sendNewResource($inventoryId, $positionId, $newItemId, $newResourceCount, $newResourceLore);
    }

    /**
     * Send exhausted resources
     *
     * @param string $inventoryId
     * @param int $positionId
     * @param int $itemCount
     * @return null
     * @throws GEOServerException
     */
    public function removeExhaustedResources(string $inventoryId, int $positionId, int $itemCount) {
        $this->sendExhaustedResources($inventoryId, $positionId, $itemCount);
    }

    /**
     * Query for inventory data
     *
     * @param string $invType
     * @return array $inventory
     * @throws GEOServerException
     */
    private function getInventoryData(string $invType):array {

        $user = new User();
        $userData = $user->setUser();

        $sql = 'SELECT *, CONCAT(`tr`.`inventory_id`, "_", `tr`.`resource_inventory_position`) as "absolute_inventory_position" FROM `tbl_resources` `tr` 
            INNER JOIN `lkp_items` `li` ON `tr`.`item_id` = `li`.`item_id`
            WHERE `tr`.`user_id` = :userId
            AND `tr`.`inventory_id` = :inventory_id';

        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute(['userId' => $userData->user_id,
                            'inventory_id' => $invType]);
            $result = $stmt->fetchAll();
            $inventory = $result ? $result : [];
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not get inventory data.', 0, $e, 3);
        }
        return $inventory;
    }

    /**
     * Save new resource counts to inventory
     *
     * @param int $totalCount
     * @param int $targetResourceId
     * @return boolean
     * @throws GEOServerException
     */
    private function sendResourceCount(int $totalCount, int $targetResourceId):bool {

        $sql = 'UPDATE `tbl_resources` SET `resource_count` = :resource_count WHERE `resource_id` = :resource_id';
        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute(['resource_id' => $targetResourceId,
                'resource_count' => $totalCount]);
            return true;
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not get inventory data.', 0, $e, 3);
        }
    }

    /**
     * Remove old stack from inventory
     *
     * @param int $droppedResourceId
     * @return null
     * @throws GEOServerException
     */
    private function removeOldStack(int $droppedResourceId) {
        $sql = 'DELETE FROM `tbl_resources` WHERE `resource_id` = :resource_id';
        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute(['resource_id' => $droppedResourceId]);
            return true;
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not get inventory data.', 0, $e, 3);
        }
    }

    /**
     * Save updated resource position to inventory
     *
     * @param string $inventoryId
     * @param int $positionId
     * @param int $droppedResourceId
     * @return null
     * @throws GEOServerException
     */
    private function sendResourcePosition(string $inventoryId, int $positionId, int $droppedResourceId) {

        $sql = 'UPDATE `tbl_resources` SET `resource_inventory_position` = :resource_inventory_position, `inventory_id` = :inventory_id WHERE `resource_id` = :resource_id';
        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute(['resource_id' => $droppedResourceId,
                'resource_inventory_position' => $positionId,
                'inventory_id' => $inventoryId]);
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not get inventory data.', 0, $e, 3);
        }
    }

    /**
     * Save new resource
     *
     * @param string $inventoryId
     * @param int $positionId
     * @param int $newItemId
     * @param int $newResourceCount
     * @param string $newResourceLore
     * @return int
     * @throws GEOServerException
     */
    private function sendNewResource(string $inventoryId, int $positionId, int $newItemId, int $newResourceCount, string $newResourceLore):int {

        $user = new User();
        $userData = $user->setUser();

        $sourceId = 0;

        $sql = 'INSERT INTO `tbl_resources` 
    (`user_id`, `source_id`, `item_id`, `resource_count`, `resource_lore`, `inventory_id`, `resource_inventory_position`) 
    VAlUES (:user_id, :source_id, :item_id, :resource_count, :resource_lore, :inventory_id, :resource_inventory_position)';
        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute([
                'user_id' => $userData->user_id,
                'source_id' => $sourceId,
                'item_id' => $newItemId,
                'resource_count' => $newResourceCount,
                'resource_lore' => $newResourceLore,
                'inventory_id' => $inventoryId,
                'resource_inventory_position' => $positionId
            ]);
            $id = $conn->lastInsertId();
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not get inventory data.', 0, $e, 3);
        }
        return $id;
    }

    private function sendExhaustedResources(string $inventoryId, int $positionId, int $itemCount) {

        $user = new User();
        $userData = $user->setUser();

        if($itemCount > 1) {
            $removeOne = 1;
            $sql = 'UPDATE `tbl_resources` SET `resource_count` = `resource_count` - :remove_one 
                    WHERE `user_id` = :user_id 
                      AND `inventory_id` = :inventory_id 
                      AND `resource_inventory_position` = :resource_inventory_position';
            try {
                $conn = Database::connectMain();
                $stmt = $conn->prepare($sql);
                $stmt->execute([
                    'remove_one' => $removeOne,
                    'user_id' => $userData->user_id,
                    'inventory_id' => $inventoryId,
                    'resource_inventory_position' => $positionId
                ]);
            } catch (\PDOException $e) {
                throw new GEOServerException('Could not get inventory data.', 0, $e, 3);
            }
        } else {
            $sql = 'DELETE FROM `tbl_resources` WHERE `user_id` = :user_id AND `inventory_id` = :inventory_id AND `resource_inventory_position` = :resource_inventory_position';
            try {
                $conn = Database::connectMain();
                $stmt = $conn->prepare($sql);
                $stmt->execute([
                    'user_id' => $userData->user_id,
                    'inventory_id' => $inventoryId,
                    'resource_inventory_position' => $positionId
                ]);
            } catch (\PDOException $e) {
                throw new GEOServerException('Could not get inventory data.', 0, $e, 3);
            }
        }
    }
}

