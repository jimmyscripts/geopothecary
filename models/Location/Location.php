<?php
class Location {

    /**
     *
     * @param float $lat
     * @param float $long
     * @param float $searchRange
     * @return array
     * @throws GEOServerException
     */
    public function getLocationsByPositRange(float $lat, float $long, float $searchRange):array {

        $lat_lower = $lat - $searchRange;
        $lat_upper = $lat + $searchRange;
        $long_lower = $long - $searchRange;
        $long_upper = $long + $searchRange;

        //First we get the location ids associated with this lat and long range.
        $sql = 'SELECT *, ROUND(`tl`.`location_lat` - :user_lat, 3) as lat_dist, 
            ROUND(`tl`.`location_long` - :user_long, 3) as long_dist 
            FROM `tbl_locations` `tl` 
            WHERE (`tl`.`location_lat` BETWEEN :lat_lower AND :lat_upper) 
            AND (`tl`.`location_long` BETWEEN :long_lower AND :long_upper)';
        try {//Find why this query is not working.
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute(['user_long' => $long, 'user_lat' => $lat, 'lat_upper' => $lat_upper, 'lat_lower' => $lat_lower, 'long_upper' => $long_upper, 'long_lower' => $long_lower]);
            $result = $stmt->fetchAll();
            $locations = $result ? $result : [];
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not get location data.', 0, $e, 3);
        }

        foreach($locations as $key => $location) {
            $locations[$key]['location_type_name'] = ucwords(str_replace("_", " ", $location['location_type']));
        }
        return $locations;
    }
}