<?php

class Alchemy extends Auth {
    /**
     * Set the pickup data array.
     *
     * @param string $tool
     * @param array $resources
     * @return array <Search>
     */
    public function setMixture(string $tool, array $resources):array {
        return $this->checkMixture($tool, $resources);
    }

    private function checkMixture($tool, $resources) {

        $itemIdOne = $resources['tool_slot_0'] ?: 0;
        $itemIdTwo = $resources['tool_slot_1'] ?: 0;
        $itemIdThree = $resources['tool_slot_2'] ?: 0;
        $itemIdFour = $resources['tool_slot_3'] ?: 0;
        $itemIdFive = $resources['tool_slot_4'] ?: 0;
        $itemIdSix = $resources['tool_slot_5'] ?: 0;
        $itemIdSeven = $resources['tool_slot_6'] ?: 0;
        $itemIdEight = $resources['tool_slot_7'] ?: 0;
        $itemIdNine = $resources['tool_slot_8'] ?: 0;

        if(!$this->auth()) {return;}

        $sql = 'SELECT * FROM `tool_bowl` WHERE (
            `item_id_one` = :item_id_one_one OR 
            `item_id_one` = :item_id_one_two OR 
            `item_id_one` = :item_id_one_three OR 
            `item_id_one` = :item_id_one_four OR 
            `item_id_one` = :item_id_one_five OR 
            `item_id_one` = :item_id_one_six OR 
            `item_id_one` = :item_id_one_seven OR 
            `item_id_one` = :item_id_one_eight OR 
            `item_id_one` = :item_id_one_nine
    ) AND (
            `item_id_two` = :item_id_two_one OR 
            `item_id_two` = :item_id_two_two OR 
            `item_id_two` = :item_id_two_three OR 
            `item_id_two` = :item_id_two_four OR 
            `item_id_two` = :item_id_two_five OR 
            `item_id_two` = :item_id_two_six OR 
            `item_id_two` = :item_id_two_seven OR 
            `item_id_two` = :item_id_two_eight OR 
            `item_id_two` = :item_id_two_nine
    ) AND (
            `item_id_three` = :item_id_three_one OR 
            `item_id_three` = :item_id_three_two OR 
            `item_id_three` = :item_id_three_three OR 
            `item_id_three` = :item_id_three_four OR 
            `item_id_three` = :item_id_three_five OR 
            `item_id_three` = :item_id_three_six OR 
            `item_id_three` = :item_id_three_seven OR 
            `item_id_three` = :item_id_three_eight OR 
            `item_id_three` = :item_id_three_nine
    ) AND (
            `item_id_four` = :item_id_four_one OR 
            `item_id_four` = :item_id_four_two OR 
            `item_id_four` = :item_id_four_three OR 
            `item_id_four` = :item_id_four_four OR 
            `item_id_five` = :item_id_four_five OR 
            `item_id_four` = :item_id_four_six OR 
            `item_id_four` = :item_id_four_seven OR 
            `item_id_four` = :item_id_four_eight OR 
            `item_id_four` = :item_id_four_nine
    ) AND (
            `item_id_five` = :item_id_five_one OR 
            `item_id_five` = :item_id_five_two OR 
            `item_id_five` = :item_id_five_three OR 
            `item_id_five` = :item_id_five_four OR 
            `item_id_five` = :item_id_five_five OR 
            `item_id_five` = :item_id_five_six OR 
            `item_id_five` = :item_id_five_seven OR 
            `item_id_five` = :item_id_five_eight OR 
            `item_id_five` = :item_id_five_nine
    ) AND (
            `item_id_six` = :item_id_six_one OR 
            `item_id_six` = :item_id_six_two OR 
            `item_id_six` = :item_id_six_three OR 
            `item_id_six` = :item_id_six_four OR 
            `item_id_six` = :item_id_six_five OR 
            `item_id_six` = :item_id_six_six OR 
            `item_id_six` = :item_id_six_seven OR 
            `item_id_six` = :item_id_six_eight OR 
            `item_id_six` = :item_id_six_nine
    ) AND (
            `item_id_seven` = :item_id_seven_one OR 
            `item_id_seven` = :item_id_seven_two OR 
            `item_id_seven` = :item_id_seven_three OR 
            `item_id_seven` = :item_id_seven_four OR 
            `item_id_seven` = :item_id_seven_five OR 
            `item_id_seven` = :item_id_seven_six OR 
            `item_id_seven` = :item_id_seven_seven OR 
            `item_id_seven` = :item_id_seven_eight OR 
            `item_id_seven` = :item_id_seven_nine
    ) AND (
            `item_id_eight` = :item_id_eight_one OR 
            `item_id_eight` = :item_id_eight_two OR 
            `item_id_eight` = :item_id_eight_three OR 
            `item_id_eight` = :item_id_eight_four OR 
            `item_id_eight` = :item_id_eight_five OR 
            `item_id_eight` = :item_id_eight_six OR 
            `item_id_eight` = :item_id_eight_seven OR 
            `item_id_eight` = :item_id_eight_eight OR 
            `item_id_eight` = :item_id_eight_nine
    ) AND (
            `item_id_nine` = :item_id_nine_one OR 
            `item_id_nine` = :item_id_nine_two OR 
            `item_id_nine` = :item_id_nine_three OR 
            `item_id_nine` = :item_id_nine_four OR 
            `item_id_nine` = :item_id_nine_five OR 
            `item_id_nine` = :item_id_nine_six OR 
            `item_id_nine` = :item_id_nine_seven OR 
            `item_id_nine` = :item_id_nine_eight OR 
            `item_id_nine` = :item_id_nine_nine
    )';

        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute([
                'item_id_one_one' => $itemIdOne,
                'item_id_one_two' => $itemIdTwo,
                'item_id_one_three' => $itemIdThree,
                'item_id_one_four' => $itemIdFour,
                'item_id_one_five' => $itemIdFive,
                'item_id_one_six' => $itemIdSix,
                'item_id_one_seven' => $itemIdSeven,
                'item_id_one_eight' => $itemIdEight,
                'item_id_one_nine' => $itemIdNine,
                'item_id_two_one' => $itemIdOne,
                'item_id_two_two' => $itemIdTwo,
                'item_id_two_three' => $itemIdThree,
                'item_id_two_four' => $itemIdFour,
                'item_id_two_five' => $itemIdFive,
                'item_id_two_six' => $itemIdSix,
                'item_id_two_seven' => $itemIdSeven,
                'item_id_two_eight' => $itemIdEight,
                'item_id_two_nine' => $itemIdNine,
                'item_id_three_one' => $itemIdOne,
                'item_id_three_two' => $itemIdTwo,
                'item_id_three_three' => $itemIdThree,
                'item_id_three_four' => $itemIdFour,
                'item_id_three_five' => $itemIdFive,
                'item_id_three_six' => $itemIdSix,
                'item_id_three_seven' => $itemIdSeven,
                'item_id_three_eight' => $itemIdEight,
                'item_id_three_nine' => $itemIdNine,
                'item_id_four_one' => $itemIdOne,
                'item_id_four_two' => $itemIdTwo,
                'item_id_four_three' => $itemIdThree,
                'item_id_four_four' => $itemIdFour,
                'item_id_four_five' => $itemIdFive,
                'item_id_four_six' => $itemIdSix,
                'item_id_four_seven' => $itemIdSeven,
                'item_id_four_eight' => $itemIdEight,
                'item_id_four_nine' => $itemIdNine,
                'item_id_five_one' => $itemIdOne,
                'item_id_five_two' => $itemIdTwo,
                'item_id_five_three' => $itemIdThree,
                'item_id_five_four' => $itemIdFour,
                'item_id_five_five' => $itemIdFive,
                'item_id_five_six' => $itemIdSix,
                'item_id_five_seven' => $itemIdSeven,
                'item_id_five_eight' => $itemIdEight,
                'item_id_five_nine' => $itemIdNine,
                'item_id_six_one' => $itemIdOne,
                'item_id_six_two' => $itemIdTwo,
                'item_id_six_three' => $itemIdThree,
                'item_id_six_four' => $itemIdFour,
                'item_id_six_five' => $itemIdFive,
                'item_id_six_six' => $itemIdSix,
                'item_id_six_seven' => $itemIdSeven,
                'item_id_six_eight' => $itemIdEight,
                'item_id_six_nine' => $itemIdNine,
                'item_id_seven_one' => $itemIdOne,
                'item_id_seven_two' => $itemIdTwo,
                'item_id_seven_three' => $itemIdThree,
                'item_id_seven_four' => $itemIdFour,
                'item_id_seven_five' => $itemIdFive,
                'item_id_seven_six' => $itemIdSix,
                'item_id_seven_seven' => $itemIdSeven,
                'item_id_seven_eight' => $itemIdEight,
                'item_id_seven_nine' => $itemIdNine,
                'item_id_eight_one' => $itemIdOne,
                'item_id_eight_two' => $itemIdTwo,
                'item_id_eight_three' => $itemIdThree,
                'item_id_eight_four' => $itemIdFour,
                'item_id_eight_five' => $itemIdFive,
                'item_id_eight_six' => $itemIdSix,
                'item_id_eight_seven' => $itemIdSeven,
                'item_id_eight_eight' => $itemIdEight,
                'item_id_eight_nine' => $itemIdNine,
                'item_id_nine_one' => $itemIdOne,
                'item_id_nine_two' => $itemIdTwo,
                'item_id_nine_three' => $itemIdThree,
                'item_id_nine_four' => $itemIdFour,
                'item_id_nine_five' => $itemIdFive,
                'item_id_nine_six' => $itemIdSix,
                'item_id_nine_seven' => $itemIdSeven,
                'item_id_nine_eight' => $itemIdEight,
                'item_id_nine_nine' => $itemIdNine
            ]);
            $potentialMixtures = $stmt->fetchAll();

        } catch (\PDOException $e) {
            throw new GEOServerException('Could not get mixture data.', 0, $e, 3);
        }

        $chosenItemId = 0;
        $thisMixtureCount = 0;
        foreach($potentialMixtures as $potentialMixture) {
            $thisMixture = $potentialMixture['item_id_result'];
            $thisMixtureCount = $potentialMixture['result_count'];
            unset($potentialMixture['bowl_id']);
            unset($potentialMixture['item_id_result']);
            unset($potentialMixture['result_count']);
            if(
                in_array($resources['tool_slot_0'], $potentialMixture) &&
                in_array($resources['tool_slot_1'], $potentialMixture) &&
                in_array($resources['tool_slot_2'], $potentialMixture) &&
                in_array($resources['tool_slot_3'], $potentialMixture) &&
                in_array($resources['tool_slot_4'], $potentialMixture) &&
                in_array($resources['tool_slot_5'], $potentialMixture) &&
                in_array($resources['tool_slot_6'], $potentialMixture) &&
                in_array($resources['tool_slot_7'], $potentialMixture) &&
                in_array($resources['tool_slot_8'], $potentialMixture)
            ) {
                $chosenItemId = $thisMixture;
            }
        }

        $sql = 'SELECT * FROM `lkp_items` WHERE `item_id` = :itemId';

        try {
            $conn = Database::connectMain();
            $stmt = $conn->prepare($sql);
            $stmt->execute(['itemId' => $chosenItemId]);
            $result = $stmt->fetchAll();
            $newItem = $result ? $result : [];
        } catch (\PDOException $e) {
            throw new GEOServerException('Could not get inventory data.', 0, $e, 3);
        }
        $newItem[0]['result_count'] = $thisMixtureCount;
        return $newItem;
    }
}