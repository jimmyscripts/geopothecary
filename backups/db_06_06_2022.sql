-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Jun 06, 2022 at 01:00 PM
-- Server version: 10.6.5-MariaDB-1:10.6.5+maria~focal
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `geopothecary`
--

-- --------------------------------------------------------

--
-- Table structure for table `lkp_items`
--

CREATE TABLE `lkp_items` (
                             `item_id` int(4) NOT NULL,
                             `item_name` varchar(60) NOT NULL,
                             `item_slug` varchar(200) NOT NULL,
                             `item_use` varchar(60) NOT NULL,
                             `item_table` varchar(60) NOT NULL,
                             `item_type` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lkp_items`
--

INSERT INTO `lkp_items` (`item_id`, `item_name`, `item_slug`, `item_use`, `item_table`, `item_type`) VALUES
(1000, 'First Item', 'blueberry', 'Power Game!', '', 'material'),
(1002, 'Second Item', 'fire', 'Second item of power.', '', 'material'),
(1003, 'Mortar & Pestle', 'mortar_pestle', 'Combining', 'tool_mortar_pestle', 'tool'),
(1004, 'Ground Cinnamon', 'ground_cinnamon', 'Spice', '', 'material'),
(1005, 'Cilantro', 'cilantro', 'Spice', '', 'material'),
(1006, 'Basil', 'basil', 'Spice', '', 'material'),
(1007, 'Purple Passion Flower (Passiflora Incarnata)', 'passiflora_incarnata', 'Herb', '', 'material'),
(1008, 'Bowl', 'bowl', 'Combining', 'tool_bowl', 'tool'),
(1009, 'Ginger (Zingiber Officinale)', 'zingiber_officinale', 'Herb', '', 'material'),
(1010, 'Alcohol', 'alcohol', 'Extraction', '', 'material'),
(1011, 'Glass Jar', 'glass_jar', 'Multiple', 'tool_jar', 'tool');

-- --------------------------------------------------------

--
-- Table structure for table `lkp_lore`
--

CREATE TABLE `lkp_lore` (
                            `lore_id` int(4) NOT NULL,
                            `lore_name` varchar(60) NOT NULL,
                            `lore_use` varchar(200) NOT NULL,
                            `lore_min` int(10) NOT NULL,
                            `lore_max` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inventory`
--

CREATE TABLE `tbl_inventory` (
                                 `inventory_id` int(12) NOT NULL,
                                 `user_id` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_inventory`
--

INSERT INTO `tbl_inventory` (`inventory_id`, `user_id`) VALUES
(1000000000, 1000000000);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_locations`
--

CREATE TABLE `tbl_locations` (
                                 `location_id` int(10) NOT NULL,
                                 `location_lat` float(15,11) NOT NULL,
  `location_long` float(15,11) NOT NULL,
  `location_name` varchar(260) DEFAULT NULL,
  `google_name` varchar(260) DEFAULT NULL,
  `google_address` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_locations`
--

INSERT INTO `tbl_locations` (`location_id`, `location_lat`, `location_long`, `location_name`, `google_name`, `google_address`) VALUES
(1000, 30.20236396790, -81.61198425293, 'Testing', NULL, NULL),
(1001, 30.20226478577, -81.62198638916, 'Testing', NULL, NULL),
(1002, 30.20226478577, -81.61178588867, 'Testing', NULL, NULL),
(1003, 30.20226478577, -81.61198425293, 'Testing', NULL, NULL),
(1004, 30.08226394653, -81.41197967529, 'Testing', NULL, NULL),
(1625, 30.22432136536, -81.51045989990, '', 'Fort Family Park', '8000 Baymeadows Rd E, Jacksonville, FL 32256'),
(1626, 30.27359580994, -81.56208801270, '', 'Brackridge Park', '8583 Newton Rd, Jacksonville, FL 32216'),
(1627, 30.17794609070, -81.56678771973, '', 'Losco Regional Park', '10931 Hood Rd, Jacksonville, FL 32257'),
(1628, 30.26536750793, -81.39597320557, '', 'South Beach Park and Sunshine Playground', '2514 S Beach Pkwy, Jacksonville Beach, FL 32250'),
(1629, 30.28425598145, -81.50239562988, '', 'Playground', '11900 Beach Blvd, Jacksonville, FL 32246'),
(1630, 30.22885322571, -81.50908660889, '', 'Deerwood Rotary Childrens Park', '7901 Baymeadows Rd E, Jacksonville, FL 32256'),
(1631, 30.27104568481, -81.59904479980, '', 'Lovelace Park', '6401 Barnes Rd S, Jacksonville, FL 32216'),
(1632, 30.27141571045, -81.59288024902, '', 'Jacksonville Drew Park', '6621 Barnes Rd S, Jacksonville, FL 32216'),
(1633, 30.24697875977, -81.62683105469, '', 'Verona Park', '2901 San Fernando Rd, Jacksonville, FL 32217'),
(1634, 30.47037506104, -81.56238555908, '', 'William F Sheffield Regional Park', '3659 New Berlin Rd, Jacksonville, FL 32226'),
(1635, 30.20238685608, -81.62525939941, '', 'Elizabeth \"Betty\" Wolfe Park', '3320 Chrysler Dr, Jacksonville, FL 32257'),
(1636, 30.30130386353, -81.63356781006, '', 'St Nicholas Playground', '2260 Spring Park Rd, Jacksonville, FL 32207'),
(1637, 30.15965461731, -81.55456542969, '', 'Palmetto Leaves Regional Park - Parking', '5760 Greenland Rd, Jacksonville, FL 32258'),
(1638, 30.29925918579, -81.71197509766, '', 'Boone Park', '3700 Park St, Jacksonville, FL 32205'),
(1639, 30.26974487305, -81.56892395020, '', 'Touchton Park', 'Jacksonville, FL 32246'),
(1640, 30.27921867371, -81.75655364990, '', 'Wiley Road Playground', '6750 Wiley Rd, Jacksonville, FL 32210'),
(1641, 30.28572273254, -81.54354858398, '', 'Forestry Tower Park', '10430 Beach Blvd, Jacksonville, FL 32246'),
(1642, 30.29651832581, -81.50497436523, '', 'Huffman Boulevard Park', '2775 Huffman Blvd, Jacksonville, FL 32246'),
(1643, 30.15650558472, -81.54387664795, '', 'Greenland Park', '11808 Fayal Dr, Jacksonville, FL 32258'),
(1644, 30.21805381775, -81.70996856689, '', 'Ortega Hills Playground', '5000 Greenway Dr N, Jacksonville, FL 32244'),
(1685, 30.22432136536, -81.51045989990, '', 'Fort Family Park', '8000 Baymeadows Rd E, Jacksonville, FL 32256, United States'),
(1686, 30.19812774658, -81.56999969482, '', 'We Rock the Spectrum Kids Gym - Jacksonville', '9357 Philips Hwy #3, Jacksonville, FL 32256, United States'),
(1687, 30.22362709045, -81.51617431641, '', 'Twin Lake ELementary Playground', '8000 Point Meadows Dr, Jacksonville, FL 32256, United States'),
(1688, 30.21711921692, -81.54024505615, '', 'Deerwood HOA Playground Pavilion', '8408-8436 Hollyridge Rd, Jacksonville, FL 32256, United States'),
(1689, 30.22885322571, -81.50908660889, '', 'Deerwood Rotary Childrens Park', '7901 Baymeadows Rd E, Jacksonville, FL 32256, United States'),
(1690, 30.22035980225, -81.58391571045, '', 'Sensory Towne', 'Centurion App Square, 8380 Baymeadows Rd #6, Jacksonville, FL 32256, United States'),
(1691, 30.27359580994, -81.56208801270, '', 'Brackridge Park', '8583 Newton Rd, Jacksonville, FL 32216, United States'),
(1692, 30.18958091736, -81.55419158936, '', 'Urban Air Trampoline and Adventure Park', '9950 Southside Blvd, Jacksonville, FL 32256, United States'),
(1693, 30.17794609070, -81.56678771973, '', 'Losco Regional Park', '10931 Hood Rd, Jacksonville, FL 32257, United States'),
(1694, 30.47455024719, -81.59158325195, '', 'Playground', '1471 Elmar Rd, Jacksonville, FL 32218, United States'),
(1695, 30.24716758728, -81.57869720459, '', 'Flight Adventure Park Jacksonville', '7022 A C Skinner Pkwy #200, Jacksonville, FL 32256, United States'),
(1696, 30.20238685608, -81.62525939941, '', 'Elizabeth \"Betty\" Wolfe Park', '3320 Chrysler Dr, Jacksonville, FL 32257, United States'),
(1697, 30.46600532532, -81.56246185303, '', 'Playground', '3659 New Berlin Rd, Jacksonville, FL 32226, United States'),
(1698, 30.24389839172, -81.60892486572, '', 'Playground', '7641 Powers Ave, Jacksonville, FL 32217, United States'),
(1699, 30.27104568481, -81.59904479980, '', 'Lovelace Park', '6401 Barnes Rd S, Jacksonville, FL 32216, United States'),
(1700, 30.23950576782, -81.57740783691, '', 'Dog Wood Park of Jacksonville', '7407 Salisbury Rd, Jacksonville, FL 32256, United States'),
(1701, 30.28425598145, -81.50239562988, '', 'Playground', '11900 Beach Blvd, Jacksonville, FL 32246, United States'),
(1702, 30.29439353943, -81.70679473877, '', 'Boone Park South Playground', '3725-3735 St Johns Ave, Jacksonville, FL 32205, United States'),
(1703, 30.22440338135, -81.69372558594, '', 'Patriots Grove Playground', '2080 Child St, Jacksonville, FL 32214, United States'),
(1704, 30.31793212891, -81.52919769287, '', 'Brookview Playground', 'Jacksonville, FL 32246, United States'),
(1906, 30.19184875488, -81.60250854492, '', 'Huntington Forest Park', '10106 Huntington Forest Blvd E, Jacksonville, FL 32257, United States'),
(1907, 30.19130134583, -81.62574768066, '', 'Bowl america', '10235 San Jose Blvd, Jacksonville, FL 32257, United States'),
(1908, 30.17653083801, -81.61881256104, '', 'Burnett Park', '3740 Burnett Park Rd, Jacksonville, FL 32257, United States'),
(1914, 30.24697875977, -81.62683105469, '', 'Verona Park', '2901 San Fernando Rd, Jacksonville, FL 32217, United States'),
(1916, 30.15490341187, -81.63604736328, '', 'Chuck Rogers Park', '11950 San Jose Blvd, Jacksonville, FL 32223, United States'),
(1917, 30.29925918579, -81.71197509766, '', 'Boone Park', '3700 Park St, Jacksonville, FL 32205, United States'),
(1920, 30.13614654541, -81.63600921631, '', 'Mandarin Park', '14780 Mandarin Rd, Jacksonville, FL 32223, United States'),
(1921, 30.14921569824, -81.63854217529, '', 'Walter Anderson Memorial Park', '2780-2818 Orange Picker Rd, Jacksonville, FL 32223, United States'),
(1922, 30.22766876221, -81.60653686523, '', 'Alejandro Garces Camp Tomahawk Park', '8419 San Ardo Dr, Jacksonville, FL 32217, United States'),
(1923, 30.28017044067, -81.63744354248, '', 'Playground', '3929 Grant Rd, Jacksonville, FL 32207, United States'),
(1924, 30.26641273499, -81.76489257812, '', 'Sweetwater Playground', '7220 Esther St, Jacksonville, FL 32210, United States'),
(1943, 30.21805381775, -81.70996856689, '', 'Ortega Hills Playground', '5000 Greenway Dr N, Jacksonville, FL 32244, United States'),
(1978, 30.26641273499, -81.76489257812, '', 'Sweetwater Playground', '7220 Esther St, Jacksonville, FL 32210'),
(1979, 30.33396911621, -81.67353057861, '', 'Florida C. Dwight Memorial Playground', '1199 Church St W, Jacksonville, FL 32204'),
(1981, 30.30379676819, -81.72611999512, '', 'Murray Drive Playground', '1187 Murray Dr, Jacksonville, FL 32205'),
(1983, 30.31708335876, -81.68090057373, '', 'Riverside Park', '753 Park St, Jacksonville, FL 32204'),
(1984, 30.44469451904, -81.63792419434, '', 'San Mateo Neighborhood Park', '11452 Renne Dr, Jacksonville, FL 32218'),
(2005, 30.40127372742, -81.64466857910, 'Jacksonville Zoo Sweet Shop', NULL, NULL),
(2006, 30.20010948181, -81.61128997803, 'Home', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_resources`
--

CREATE TABLE `tbl_resources` (
                                 `resource_id` int(120) NOT NULL,
                                 `user_id` int(12) NOT NULL,
                                 `source_id` int(12) NOT NULL,
                                 `item_id` int(4) NOT NULL,
                                 `resource_count` int(4) NOT NULL,
                                 `resource_lore` varchar(500) NOT NULL DEFAULT '[]',
                                 `inventory_id` varchar(12) NOT NULL,
                                 `resource_inventory_position` int(4) NOT NULL,
                                 `resource_active` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_resources`
--

INSERT INTO `tbl_resources` (`resource_id`, `user_id`, `source_id`, `item_id`, `resource_count`, `resource_lore`, `inventory_id`, `resource_inventory_position`, `resource_active`) VALUES
(100010, 1, 0, 1002, 12, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 2, 0),
(100012, 1, 0, 1000, 126, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 1, 0),
(100034, 1, 10004, 1000, 29, '[\'smart\' => 4, \'power\' => 2]', 'personal', 0, 0),
(100060, 1, 10002, 1002, 22, '[\'smart\' => 4, \'power\' => 2]', 'personal', 3, 0),
(100158, 1, 10003, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'hotBar', 0, 0),
(100159, 1, 10004, 1000, 1, '[\'smart\' => 4, \'power\' => 2]', 'hotBar', 1, 0),
(100160, 1, 10006, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'hotBar', 2, 0),
(100161, 1, 10007, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'hotBar', 3, 0),
(100167, 1, 10004, 1000, 1, '[\'smart\' => 4, \'power\' => 2]', 'personal', 7, 0),
(100171, 1, 10002, 1002, 1, '[\'smart\' => 4, \'power\' => 2]', 'personal', 11, 0),
(100172, 1, 10006, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 12, 0),
(100173, 1, 10007, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 13, 0),
(100175, 1, 10002, 1002, 1, '[\'smart\' => 4, \'power\' => 2]', 'personal', 15, 0),
(100179, 1, 10002, 1002, 1, '[\'smart\' => 4, \'power\' => 2]', 'personal', 16, 0),
(100190, 1, 10005, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 14, 0),
(100191, 1, 10002, 1002, 1, '[\'smart\' => 4, \'power\' => 2]', 'personal', 17, 0),
(100663, 12, 0, 1000, 79, '[]', 'personal', 1, 0),
(100672, 12, 10002, 1002, 27, '[\'smart\' => 4, \'power\' => 2]', 'hotBar', 0, 0),
(100680, 12, 0, 1006, 12, '[]', 'hotBar', 4, 0),
(100685, 12, 0, 1004, 811, '[]', 'personal', 0, 0),
(100711, 12, 0, 1005, 18, '[]', 'hotBar', 2, 0),
(100734, 12, 10011, 1011, 10, '[]', 'hotBar', 5, 0),
(100738, 12, 10012, 1007, 9, '[]', 'hotBar', 1, 0),
(100751, 12, 10003, 1000, 192, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 3, 0),
(100753, 12, 10009, 1003, 8, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sources`
--

CREATE TABLE `tbl_sources` (
                               `source_id` int(12) NOT NULL,
                               `location_id` int(11) NOT NULL,
                               `item_id` int(4) NOT NULL,
                               `source_count` int(4) NOT NULL DEFAULT 1,
                               `source_lore` varchar(500) DEFAULT NULL,
                               `source_rarity` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_sources`
--

INSERT INTO `tbl_sources` (`source_id`, `location_id`, `item_id`, `source_count`, `source_lore`, `source_rarity`) VALUES
(10001, 1000, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 4),
(10002, 1001, 1002, 1, '[\'smart\' => 4, \'power\' => 2]', 1),
(10003, 1000, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 1),
(10004, 1001, 1000, 1, '[\'smart\' => 4, \'power\' => 2]', 1),
(10005, 1000, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 4),
(10006, 1002, 1000, 10, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 4),
(10007, 1003, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 4),
(10008, 1004, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 4),
(10009, 1003, 1003, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 1),
(10010, 2005, 1007, 1, '[]', 3),
(10011, 2006, 1011, 1, '[]', 1),
(10012, 2006, 1007, 1, '[]', 1),
(10013, 1000, 1005, 2, '[]', 3),
(10014, 1635, 1007, 1, '[]', 10);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_visits`
--

CREATE TABLE `tbl_visits` (
                              `visit_id` int(30) NOT NULL,
                              `user_id` int(120) NOT NULL,
                              `location_id` int(10) NOT NULL,
                              `source_id` int(120) NOT NULL,
                              `visit_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_visits`
--

INSERT INTO `tbl_visits` (`visit_id`, `user_id`, `location_id`, `source_id`, `visit_date`) VALUES
(317, 12, 1000, 10003, '2022-01-28 03:42:03'),
(318, 12, 1001, 10004, '2022-01-28 03:42:05'),
(319, 12, 1002, 10006, '2022-01-28 03:42:06'),
(320, 12, 1003, 10007, '2022-01-28 03:42:07'),
(321, 12, 1004, 10008, '2022-01-28 03:42:09'),
(322, 12, 1000, 10003, '2022-02-20 16:10:21'),
(323, 12, 1001, 10002, '2022-02-20 16:10:23'),
(324, 12, 1002, 10006, '2022-02-20 16:10:24'),
(325, 12, 1003, 10009, '2022-02-20 16:10:25'),
(353, 12, 1000, 10003, '2022-02-23 12:49:41'),
(354, 12, 1001, 10002, '2022-02-23 12:49:42'),
(355, 12, 1002, 10006, '2022-02-23 12:49:43'),
(356, 12, 1003, 10009, '2022-02-23 12:49:44'),
(357, 12, 1000, 10003, '2022-03-08 12:45:57'),
(358, 12, 1001, 10002, '2022-03-08 12:45:59'),
(359, 12, 1002, 10006, '2022-03-08 12:46:00'),
(360, 12, 1003, 10009, '2022-03-08 12:46:01'),
(361, 12, 1000, 10001, '2022-05-08 18:48:52'),
(362, 12, 1001, 10002, '2022-05-08 18:48:53'),
(363, 12, 1002, 10006, '2022-05-08 18:48:55'),
(364, 12, 1003, 10009, '2022-05-08 18:48:56'),
(365, 12, 1000, 10003, '2022-05-10 02:51:37'),
(366, 12, 1001, 10002, '2022-05-10 02:51:38'),
(367, 12, 1002, 10006, '2022-05-10 02:51:40'),
(368, 12, 1003, 10009, '2022-05-10 02:51:41'),
(369, 12, 1000, 10001, '2022-05-14 20:43:26'),
(370, 12, 1001, 10002, '2022-05-14 20:43:28'),
(371, 12, 1002, 10006, '2022-05-14 20:43:30'),
(372, 12, 1003, 10009, '2022-05-14 20:43:31'),
(382, 12, 1000, 10003, '2022-05-15 22:57:10'),
(383, 12, 1002, 10006, '2022-05-15 22:57:11'),
(384, 12, 1003, 10009, '2022-05-15 22:57:13'),
(385, 12, 2006, 10011, '2022-05-15 22:57:14'),
(400, 12, 1000, 10003, '2022-05-25 03:04:37'),
(401, 12, 1002, 10006, '2022-05-25 03:04:40'),
(402, 12, 1003, 10009, '2022-05-25 03:08:58'),
(403, 12, 2006, 10012, '2022-05-25 03:09:01'),
(416, 12, 1000, 10003, '2022-06-05 12:05:46'),
(417, 12, 1002, 10006, '2022-06-05 12:05:48'),
(418, 12, 1003, 10009, '2022-06-05 12:05:51'),
(419, 12, 2006, 10011, '2022-06-05 12:05:55');

-- --------------------------------------------------------

--
-- Table structure for table `tool_bowl`
--

CREATE TABLE `tool_bowl` (
                             `bowl_id` int(4) NOT NULL,
                             `item_id_one` int(4) NOT NULL DEFAULT 0,
                             `item_id_two` int(4) NOT NULL DEFAULT 0,
                             `item_id_three` int(4) NOT NULL DEFAULT 0,
                             `item_id_four` int(4) NOT NULL DEFAULT 0,
                             `item_id_five` int(4) NOT NULL DEFAULT 0,
                             `item_id_six` int(4) NOT NULL DEFAULT 0,
                             `item_id_seven` int(4) NOT NULL DEFAULT 0,
                             `item_id_eight` int(4) NOT NULL DEFAULT 0,
                             `item_id_nine` int(4) NOT NULL DEFAULT 0,
                             `item_id_result` int(4) NOT NULL,
                             `result_count` int(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tool_bowl`
--

INSERT INTO `tool_bowl` (`bowl_id`, `item_id_one`, `item_id_two`, `item_id_three`, `item_id_four`, `item_id_five`, `item_id_six`, `item_id_seven`, `item_id_eight`, `item_id_nine`, `item_id_result`, `result_count`) VALUES
(1000, 1000, 0, 0, 0, 0, 0, 0, 0, 0, 1004, 3),
(1001, 1003, 0, 0, 0, 0, 0, 0, 0, 0, 1005, 1),
(1002, 1003, 1000, 0, 0, 0, 0, 0, 0, 0, 1006, 1),
(1004, 1004, 0, 0, 0, 0, 0, 0, 0, 0, 1000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
                         `user_id` int(20) NOT NULL,
                         `user_name` varchar(120) NOT NULL,
                         `user_email` varchar(255) NOT NULL,
                         `user_password` varchar(255) NOT NULL,
                         `user_activate` varchar(255) DEFAULT NULL,
                         `user_role` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_email`, `user_password`, `user_activate`, `user_role`) VALUES
(12, 'jimmyscripts', 'jon@doe.com', '$2y$10$4QEUTaq1dCI8Pa/nPUuVfuciM7Dk2.6qZg0o0wB/0aay6ud3RjXJ.', '23af785dd0eac06bf0e21ae6109dd3f1', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lkp_items`
--
ALTER TABLE `lkp_items`
    ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `lkp_lore`
--
ALTER TABLE `lkp_lore`
    ADD PRIMARY KEY (`lore_id`);

--
-- Indexes for table `tbl_locations`
--
ALTER TABLE `tbl_locations`
    ADD PRIMARY KEY (`location_id`),
  ADD UNIQUE KEY `unique_address` (`google_address`);

--
-- Indexes for table `tbl_resources`
--
ALTER TABLE `tbl_resources`
    ADD PRIMARY KEY (`resource_id`);

--
-- Indexes for table `tbl_sources`
--
ALTER TABLE `tbl_sources`
    ADD PRIMARY KEY (`source_id`);

--
-- Indexes for table `tbl_visits`
--
ALTER TABLE `tbl_visits`
    ADD PRIMARY KEY (`visit_id`);

--
-- Indexes for table `tool_bowl`
--
ALTER TABLE `tool_bowl`
    ADD PRIMARY KEY (`bowl_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
    ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lkp_items`
--
ALTER TABLE `lkp_items`
    MODIFY `item_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1012;

--
-- AUTO_INCREMENT for table `lkp_lore`
--
ALTER TABLE `lkp_lore`
    MODIFY `lore_id` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_locations`
--
ALTER TABLE `tbl_locations`
    MODIFY `location_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2027;

--
-- AUTO_INCREMENT for table `tbl_resources`
--
ALTER TABLE `tbl_resources`
    MODIFY `resource_id` int(120) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100755;

--
-- AUTO_INCREMENT for table `tbl_sources`
--
ALTER TABLE `tbl_sources`
    MODIFY `source_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10015;

--
-- AUTO_INCREMENT for table `tbl_visits`
--
ALTER TABLE `tbl_visits`
    MODIFY `visit_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=420;

--
-- AUTO_INCREMENT for table `tool_bowl`
--
ALTER TABLE `tool_bowl`
    MODIFY `bowl_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1005;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
    MODIFY `user_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
