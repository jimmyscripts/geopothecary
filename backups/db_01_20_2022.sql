-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Jan 20, 2022 at 09:53 PM
-- Server version: 10.6.4-MariaDB-1:10.6.4+maria~focal
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `geopothecary`
--

-- --------------------------------------------------------

--
-- Table structure for table `lkp_items`
--

CREATE TABLE `lkp_items` (
  `item_id` int(4) NOT NULL,
  `item_name` varchar(60) NOT NULL,
  `item_slug` varchar(200) NOT NULL,
  `item_use` varchar(60) NOT NULL,
  `item_table` varchar(60) NOT NULL,
  `item_type` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lkp_items`
--

INSERT INTO `lkp_items` (`item_id`, `item_name`, `item_slug`, `item_use`, `item_table`, `item_type`) VALUES
(1000, 'First Item', 'blueberry', 'Power Game!', '', 'material'),
(1002, 'Second Item', 'fire', 'Second item of power.', '', 'material'),
(1003, 'First Tool', 'mortar_pestle', '', 'tbl_mortar_pestle', 'tool'),
(1004, 'Ground Cinnamon', 'ground_cinnamon', 'Spice', '', 'material');

-- --------------------------------------------------------

--
-- Table structure for table `lkp_lore`
--

CREATE TABLE `lkp_lore` (
  `lore_id` int(4) NOT NULL,
  `lore_name` varchar(60) NOT NULL,
  `lore_use` varchar(200) NOT NULL,
  `lore_min` int(10) NOT NULL,
  `lore_max` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inventory`
--

CREATE TABLE `tbl_inventory` (
  `inventory_id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_inventory`
--

INSERT INTO `tbl_inventory` (`inventory_id`, `user_id`) VALUES
(1000000000, 1000000000);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_locations`
--

CREATE TABLE `tbl_locations` (
  `location_id` int(10) NOT NULL,
  `location_lat` float(15,11) NOT NULL,
  `location_long` float(15,11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_locations`
--

INSERT INTO `tbl_locations` (`location_id`, `location_lat`, `location_long`) VALUES
(1000, 30.20226478577, -81.61198425293),
(1001, 30.20226478577, -81.61198425293),
(1002, 30.20226478577, -81.61198425293),
(1003, 30.20226478577, -81.61198425293),
(1004, 30.08226394653, -81.41197967529);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_resources`
--

CREATE TABLE `tbl_resources` (
  `resource_id` int(120) NOT NULL,
  `user_id` int(12) NOT NULL,
  `source_id` int(12) NOT NULL,
  `item_id` int(4) NOT NULL,
  `resource_count` int(4) NOT NULL,
  `resource_lore` varchar(500) NOT NULL,
  `inventory_id` varchar(12) NOT NULL,
  `resource_inventory_position` int(4) NOT NULL,
  `resource_active` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_resources`
--

INSERT INTO `tbl_resources` (`resource_id`, `user_id`, `source_id`, `item_id`, `resource_count`, `resource_lore`, `inventory_id`, `resource_inventory_position`, `resource_active`) VALUES
(100010, 1, 0, 1002, 12, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 2, 0),
(100012, 1, 0, 1000, 126, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 1, 0),
(100034, 1, 10004, 1000, 29, '[\'smart\' => 4, \'power\' => 2]', 'personal', 0, 0),
(100060, 1, 10002, 1002, 22, '[\'smart\' => 4, \'power\' => 2]', 'personal', 3, 0),
(100158, 1, 10003, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'hotBar', 0, 0),
(100159, 1, 10004, 1000, 1, '[\'smart\' => 4, \'power\' => 2]', 'hotBar', 1, 0),
(100160, 1, 10006, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'hotBar', 2, 0),
(100161, 1, 10007, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'hotBar', 3, 0),
(100162, 1, 10003, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'hotBar', 4, 0),
(100163, 1, 10002, 1002, 1, '[\'smart\' => 4, \'power\' => 2]', 'hotBar', 5, 0),
(100164, 1, 10006, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 4, 0),
(100165, 1, 10007, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 5, 0),
(100166, 1, 10003, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 6, 0),
(100167, 1, 10004, 1000, 1, '[\'smart\' => 4, \'power\' => 2]', 'personal', 7, 0),
(100168, 1, 10006, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 8, 0),
(100169, 1, 10007, 1000, 2, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 9, 0),
(100170, 1, 10003, 1000, 4, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 10, 0),
(100171, 1, 10002, 1002, 1, '[\'smart\' => 4, \'power\' => 2]', 'personal', 11, 0),
(100172, 1, 10006, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 12, 0),
(100173, 1, 10007, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 13, 0),
(100175, 1, 10002, 1002, 1, '[\'smart\' => 4, \'power\' => 2]', 'personal', 15, 0),
(100179, 1, 10002, 1002, 1, '[\'smart\' => 4, \'power\' => 2]', 'personal', 16, 0),
(100187, 12, 10004, 1000, 8, '[\'smart\' => 4, \'power\' => 2]', 'personal', 3, 0),
(100190, 1, 10005, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 14, 0),
(100191, 1, 10002, 1002, 1, '[\'smart\' => 4, \'power\' => 2]', 'personal', 17, 0),
(100234, 12, 10002, 1002, 5, '[\'smart\' => 4, \'power\' => 2]', 'personal', 1, 0),
(100241, 12, 10003, 1000, 15, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'bowl', 7, 0),
(100256, 12, 10009, 1003, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'hotBar', 4, 0),
(100260, 12, 10009, 1003, 10, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 7, 0),
(100261, 12, 10005, 1000, 4, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 8, 0),
(100262, 12, 10002, 1002, 2, '[\'smart\' => 4, \'power\' => 2]', 'hotBar', 1, 0),
(100264, 12, 10006, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'hotBar', 5, 0),
(100269, 12, 10009, 1003, 2, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'hotBar', 3, 0),
(100270, 12, 10003, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'hotBar', 0, 0),
(100271, 12, 10002, 1002, 1, '[\'smart\' => 4, \'power\' => 2]', 'hotBar', 2, 0),
(100272, 12, 10006, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 0, 0),
(100273, 12, 10009, 1003, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 'personal', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sources`
--

CREATE TABLE `tbl_sources` (
  `source_id` int(12) NOT NULL,
  `location_id` int(11) NOT NULL,
  `item_id` int(4) NOT NULL,
  `source_count` int(4) NOT NULL DEFAULT 1,
  `source_lore` varchar(500) NOT NULL,
  `source_rarity` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_sources`
--

INSERT INTO `tbl_sources` (`source_id`, `location_id`, `item_id`, `source_count`, `source_lore`, `source_rarity`) VALUES
(10001, 1000, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 4),
(10002, 1001, 1002, 1, '[\'smart\' => 4, \'power\' => 2]', 1),
(10003, 1000, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 1),
(10004, 1001, 1000, 1, '[\'smart\' => 4, \'power\' => 2]', 1),
(10005, 1000, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 4),
(10006, 1002, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 4),
(10007, 1003, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 4),
(10008, 1004, 1000, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 4),
(10009, 1003, 1003, 1, '[\'name\' => \'Erebor\', \'smart\' => 4, \'fire\' => 2]', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_visits`
--

CREATE TABLE `tbl_visits` (
  `visit_id` int(30) NOT NULL,
  `user_id` int(120) NOT NULL,
  `location_id` int(10) NOT NULL,
  `source_id` int(120) NOT NULL,
  `visit_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_visits`
--

INSERT INTO `tbl_visits` (`visit_id`, `user_id`, `location_id`, `source_id`, `visit_date`) VALUES
(290, 12, 1000, 10003, '2022-01-09 12:48:38'),
(291, 12, 1001, 10002, '2022-01-09 12:48:41'),
(292, 12, 1002, 10006, '2022-01-09 12:48:44'),
(293, 12, 1003, 10009, '2022-01-09 12:48:46'),
(294, 12, 1000, 10003, '2022-01-16 11:36:22'),
(295, 12, 1001, 10002, '2022-01-16 11:36:23'),
(296, 12, 1002, 10006, '2022-01-16 11:36:25'),
(297, 12, 1003, 10009, '2022-01-16 11:36:26');

-- --------------------------------------------------------

--
-- Table structure for table `tool_bowl`
--

CREATE TABLE `tool_bowl` (
  `bowl_id` int(4) NOT NULL,
  `item_id_one` int(4) NOT NULL DEFAULT 0,
  `item_id_two` int(4) NOT NULL DEFAULT 0,
  `item_id_three` int(4) NOT NULL DEFAULT 0,
  `item_id_four` int(4) NOT NULL DEFAULT 0,
  `item_id_five` int(4) NOT NULL DEFAULT 0,
  `item_id_six` int(4) NOT NULL DEFAULT 0,
  `item_id_seven` int(4) NOT NULL DEFAULT 0,
  `item_id_eight` int(4) NOT NULL DEFAULT 0,
  `item_id_nine` int(4) NOT NULL DEFAULT 0,
  `item_id_result` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tool_bowl`
--

INSERT INTO `tool_bowl` (`bowl_id`, `item_id_one`, `item_id_two`, `item_id_three`, `item_id_four`, `item_id_five`, `item_id_six`, `item_id_seven`, `item_id_eight`, `item_id_nine`, `item_id_result`) VALUES
(1000, 1000, 0, 0, 0, 0, 0, 0, 0, 0, 1004),
(1001, 1003, 0, 0, 0, 0, 0, 0, 0, 0, 1005),
(1002, 1003, 1000, 0, 0, 0, 0, 0, 0, 0, 1006);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(20) NOT NULL,
  `user_name` varchar(120) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_activate` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_email`, `user_password`, `user_activate`) VALUES
(12, 'jimmyscripts', 'jon@doe.com', '$2y$10$4QEUTaq1dCI8Pa/nPUuVfuciM7Dk2.6qZg0o0wB/0aay6ud3RjXJ.', '23af785dd0eac06bf0e21ae6109dd3f1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lkp_items`
--
ALTER TABLE `lkp_items`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `lkp_lore`
--
ALTER TABLE `lkp_lore`
  ADD PRIMARY KEY (`lore_id`);

--
-- Indexes for table `tbl_locations`
--
ALTER TABLE `tbl_locations`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `tbl_resources`
--
ALTER TABLE `tbl_resources`
  ADD PRIMARY KEY (`resource_id`);

--
-- Indexes for table `tbl_sources`
--
ALTER TABLE `tbl_sources`
  ADD PRIMARY KEY (`source_id`);

--
-- Indexes for table `tbl_visits`
--
ALTER TABLE `tbl_visits`
  ADD PRIMARY KEY (`visit_id`);

--
-- Indexes for table `tool_bowl`
--
ALTER TABLE `tool_bowl`
  ADD PRIMARY KEY (`bowl_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lkp_items`
--
ALTER TABLE `lkp_items`
  MODIFY `item_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1005;

--
-- AUTO_INCREMENT for table `lkp_lore`
--
ALTER TABLE `lkp_lore`
  MODIFY `lore_id` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_locations`
--
ALTER TABLE `tbl_locations`
  MODIFY `location_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1005;

--
-- AUTO_INCREMENT for table `tbl_resources`
--
ALTER TABLE `tbl_resources`
  MODIFY `resource_id` int(120) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100274;

--
-- AUTO_INCREMENT for table `tbl_sources`
--
ALTER TABLE `tbl_sources`
  MODIFY `source_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10010;

--
-- AUTO_INCREMENT for table `tbl_visits`
--
ALTER TABLE `tbl_visits`
  MODIFY `visit_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=298;

--
-- AUTO_INCREMENT for table `tool_bowl`
--
ALTER TABLE `tool_bowl`
  MODIFY `bowl_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1004;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
