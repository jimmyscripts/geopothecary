<!-- General Modal -->
<div class="modal fade" id="generalModal" tabindex="-1" aria-labelledby="generalModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <div id="modalHeader"></div>
                <button type="button" id="closeInventory" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="modalBody"></div>
            </div>
            <div class="modal-footer">
                <div id="modalFooter"></div>
            </div>
        </div>
    </div>
</div>

