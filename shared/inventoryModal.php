<?php

try {
    $inventory = new Inventory();
    $personalInventoryObj = $inventory->setInventoryData('personal');
    $hotBarInventoryObj = $inventory->setInventoryData('hotBar');
} catch (GEORequestException $e) {
    header('X-CLIENT-ERR-MSG: ' . $e->getMessage());
    header('HTTP/1.1 400');
    return;
} catch (GEOServerException $e) {
    header('HTTP/1.1 500');
    exit();
}

$inventoryCount = 360;
$hotBarCount = 6;
?>
<style>
    .popover { width: 400px; }
</style>
<!-- Modal -->
<div class="modal fade" id="inventoryModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="closeInventory" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="modalTool"></div>
                <div id="inventoryContainer">
                    <div id="inventoryTableWrapper" style="height: 283px; overflow-y: scroll;">
                        <table id="inventoryTable" class="text-center inventory-table" style="width: 100%;">
                            <?php for($i = 0; $i < $inventoryCount; $i++) : ?>
                                <?php $uid = getUniqueId(); ?>
                                <?php
                                    $makeTableRow = fmod($i, 6);
                                    $toolName = 'personal';
                                    $inventoryLoc = $toolName . '_' . $i;
                                    $filterItem = array_filter($personalInventoryObj, function($itemFiltered) use ($inventoryLoc) {
                                        return $itemFiltered['absolute_inventory_position'] == $inventoryLoc;
                                    });
                                    $valuesItem = array_values($filterItem);
                                    $thisItem = $valuesItem[0];

                                    $leftClickPopover = "<div><strong class='btn btn-danger mb-2 float-end closePop'>X</strong></div>";

                                    switch ($thisItem['item_type']) {
                                        case 'material':
                                            $leftClickPopover .= "<div><strong class='btn btn-primary w-100'>Consume</strong></div>";
                                            break;
                                        case 'tool':
                                            $leftClickPopover .= "<div><strong class='btn btn-primary w-100'>Install</strong></div>";
                                            break;
                                        default:
                                            $leftClickPopover .= "";
                                    }
                                ?>
                                <?php if($makeTableRow == 0) : ?>
                                    <tr>
                                <?php endif; ?>
                                    <td style="width:16.667%;">
                                        <section id="catcher_<?= $inventoryLoc ?>" class="item_catcher" ondrop="drop_handler(event)" ondragover="dragover_handler(event)"
                                                 data-inventory-location="<?= $inventoryLoc ?>" data-tool-name="personal">
                                            <?php if(!empty($thisItem)) : ?>
                                                <div id="<?= $uid ?>" class="item" data-item="<?= $thisItem['item_slug'] ?>" data-lore="<?= $thisItem['resource_lore'] ?>"
                                                     data-count="<?= $thisItem['resource_count'] ?>" draggable="true" ondragend="dragend_handler(event)"
                                                     data-resource-id="<?= $thisItem['resource_id'] ?>" data-item-id="<?= $thisItem['item_id'] ?>"
                                                     data-last-location="<?= $toolName ?>" data-bs-toggle="popover" title="<?= $thisItem['item_name'] ?>"
                                                     data-bs-content="<?= $leftClickPopover; ?>" onclick="popover(this)" data-bs-html="true">
                                                    <img height="45px" id="" class="inventoryItem" data-item="<?= $thisItem['item_slug'] ?>"
                                                         src="./shared/svg/<?= $thisItem['item_slug'] ?>.svg"/>
                                                    <div class="item_counter"><strong><span id="count_<?= $uid ?>"><?= $thisItem['resource_count'] ?></span></strong></div>
                                                </div>
                                            <?php endif; ?>
                                        </section>
                                    </td>
                                <?php if($makeTableRow == 5) : ?>
                                    </tr>
                                <?php endif; ?>
                            <?php endfor; ?>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <table id="inventoryHotBar" class="text-center personal-table" style="width: 100%;">
                    <?php for($i = 0; $i < $hotBarCount; $i++) : ?>
                        <?php $uid = getUniqueId(); ?>
                        <?php
                            $makeTableRow = fmod($i, 6);
                            $toolName = 'hotBar';
                            $inventoryLoc = $toolName . '_' . $i;
                            $filterItem = array_filter($hotBarInventoryObj, function($itemFiltered) use ($inventoryLoc) {
                                return $itemFiltered['absolute_inventory_position'] == $inventoryLoc;
                            });
                            $valuesItem = array_values($filterItem);
                            $thisItem = $valuesItem[0];

                            $leftClickPopover = "<div><strong class='btn btn-danger mb-2 float-end closePop'>X</strong></div>";

                            switch ($thisItem['item_type']) {
                                case 'material':
                                    $leftClickPopover .= "<div><strong class='btn btn-primary w-100'>Consume</strong></div>";
                                    break;
                                case 'tool':
                                    $leftClickPopover .= "<div><strong class='btn btn-primary w-100'>Install</strong></div>";
                                    break;
                                default:
                                    $leftClickPopover .= "";
                            }
                        ?>
                        <?php if($makeTableRow == 0) : ?>
                            <tr>
                        <?php endif; ?>
                        <td style="width:16.667%;">
                            <section id="catcher_<?= $inventoryLoc ?>" class="item_catcher" ondrop="drop_handler(event)" ondragover="dragover_handler(event)"
                                     data-inventory-location="<?= $inventoryLoc ?>" data-tool-name="hotBar">
                                <?php if(!empty($thisItem)) : ?>
                                    <div id="<?= $uid ?>" class="item" data-item="<?= $thisItem['item_slug'] ?>" data-lore="<?= $thisItem['resource_lore'] ?>"
                                         data-count="<?= $thisItem['resource_count'] ?>" draggable="true" ondragend="dragend_handler(event)"
                                         data-resource-id="<?= $thisItem['resource_id'] ?>" data-item-id="<?= $thisItem['item_id'] ?>"
                                         data-last-location="<?= $toolName ?>" data-bs-toggle="popover" title="<?= $thisItem['item_name'] ?>"
                                         data-bs-content="<?= $leftClickPopover; ?>" onclick="popover(this)" data-bs-html="true">
                                        <img height="45px" id="" class="inventoryItem" data-item="<?= $thisItem['item_slug'] ?>"
                                             src="./shared/svg/<?= $thisItem['item_slug'] ?>.svg"/>
                                        <div class="item_counter"><strong><span id="count_<?= $uid ?>"><?= $thisItem['resource_count'] ?></span></strong></div>
                                    </div>
                                <?php endif; ?>
                            </section>
                        </td>
                        <?php if($makeTableRow == 5) : ?>
                            </tr>
                        <?php endif; ?>
                    <?php endfor; ?>
                </table>
            </div>
        </div>
    </div>
</div>
