<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

require_once($_SERVER['DOCUMENT_ROOT'] . '/controllers/autoload.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/models/autoload.php');

$postContent = trim(file_get_contents('php://input'));
$values = json_decode($postContent, true);

if (!isset($values['action'])) {
    header("HTTP/1.1 500 Action option is required.");
    exit;
}

if($values['role_required'] != $values['session_role']) {
    header('HTTP/1.1 500 ' . $values['role_required'] . ' is required.');
    exit;
}

if(!session_id() || session_id() == ''){
    session_start();
}

try {
    $user = new User();
    $userData = $user->setUser();

    $values['user'] = $userData;

    $router = new ControllerFactory($values);
    $controller = $router->goController();
    $controller->getResponse();
} catch (GEORequestException $e) {
    header('X-CLIENT-ERR-MSG: ' . $e->getMessage());
    header('HTTP/1.1 400');
    exit;
} catch (GEOServerException $e) {
    header('HTTP/1.1 500');
    exit;
} catch (\Exception $e) {
    header("HTTP/1.1 500 " . $e->getMessage());
    exit();
}