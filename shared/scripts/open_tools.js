document.addEventListener('click', async e => {
    if(e.target.id == 'openMixingBowl') {

        const bowlCatcherCount = 9;

        const id = 'inventoryModal';
        const purpose = 'bowl';
        const header = ``;
        const footer = ``;
        const body = ``;

        const toolInv = await toolAsync(purpose);

        let tool = ``;
        tool += `<form id="bowl" class="tool_form row m-0">
            <table id="toolMixingBowl" class="text-center tool-table col-md-6">`;
            for(let i = 0; i < bowlCatcherCount; i++) {
                const inventoryLoc = purpose + `_` + i;
                const catcherId = 'catcher_' + purpose + '_' + i;
                let filterItem = toolInv.filter(filterItems => filterItems['absolute_inventory_position'] == inventoryLoc);

                if(i % 3 == 0) {tool += `<tr>`;}
                let filterItemId = 0;
                if(filterItem.length > 0) {
                    filterItemId = filterItem[0]['item_id'];
                }
                tool += `<td style="width: 33.333%;">
                    <input type="hidden" name="tool_slot_${i}" id="tool_slot_${i}" value="${filterItemId}" />
                    <section id="${catcherId}" class="item_catcher tool_catcher_slot" ondrop="drop_handler(event)" ondragover="dragover_handler(event)" 
                    data-inventory-location="bowl_${i}" data-tool-name="${purpose}">`;
                    if(filterItem.length > 0) {
                        filterItem = filterItem[0];
                        tool += buildItemElement(filterItem, purpose);
                    }
                tool += `</section></td>`;
                if(i % 3 == 2) {tool += `<tr>`;}
            }
        tool += `</table>`;
        tool += buildVessel();
        tool += `</form><hr>`;
        const position = ``;
        const size = ``;

        buildModal(id, purpose, header, body, footer, tool, position, size);
        await run_tool_function(purpose);
    }
});

const buildVessel = () => {
    const vesselTable = `<table id="toolResult" class="text-center result-table col-md-6">
                            <tr> <td></td> <td></td> <td></td> </tr>
                            <tr> <td></td> <td></td>
                                <td id="delivery" style="width: 33.333%; height: 33.333%;">
                                    <section id="vessel" class="item_catcher vessel_delivery" data-tool-name="vessel"></section>
                                </td>
                            </tr>
                            <tr> <td></td> <td></td> <td></td> </tr>
                        </table>`;
    return vesselTable;
}

const toolAsync = async (tool) => {
    let dataValues = {'tool' : tool};

    const data = {
        action:'get-tool-data',
        controller:'ToolController',
        output:'JSON',
        data: JSON.stringify(dataValues)
    }

    const url = `/shared/post.php`;
    const response = await fetch(url, {
        method: 'POST',
        mode: "same-origin",
        credentials: 'same-origin',
        headers: {'Content-Type':'application/json'},
        body: JSON.stringify(data)
    });
    if(!checkResponseErrors(response)){
        return;
    }

    const searchResponse = await response.json();
    const responseItems = searchResponse.responseData;

    return responseItems;
}

