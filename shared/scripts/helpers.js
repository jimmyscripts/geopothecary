const checkResponseErrors = (response) => {
    if(response.status !== 200) {
        if(response.status === 400) {
            const clientErrMsg = response.headers.get('X-CLIENT-ERR-MSG');
            alert(clientErrMsg);
            return false;
        } else if (response.status === 500) {
            alert('There was a server problem. Please contact support');
            return false;
        }else{
            alert('There was an unexpected problem. Please contact support');
            return false;
        }
    }
    return true;
}

const checkResponseErrorsModal = (response) => {
    if(response.status !== 200) {
        if(response.status === 400) {
            const clientErrMsg = response.headers.get('X-CLIENT-ERR-MSG');
            hideModal('generalModal');
            buildModal('generalModal', 'errors', '<h4>Sorry!</h4>', clientErrMsg);
            return false;
        } else if (response.status === 500) {
            alert('There was a server problem. Please contact support');
            return false;
        }else{
            alert('There was an unexpected problem. Please contact support');
            return false;
        }
    }
    return true;
}

const hideModal = (modalId) => {
    const thisModal = document.getElementById(modalId);
    const modal = bootstrap.Modal.getInstance(thisModal);
    modal.dispose();
}

const getUserId = () => {
    return '<?php $_SESSION[user_id]; ?>';
}

const generateUid = (length = 32) => {
    // Declare all characters
    let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    // Pick characers randomly
    let str = '';
    for (let i = 0; i < length; i++) {
        str += chars.charAt(Math.floor(Math.random() * chars.length));
    }

    return str;
}