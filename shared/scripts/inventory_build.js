
document.addEventListener('click', async (e) => {
    if(e.target.id == 'loadInventoryDep') {
        let dataValues = {};

        const data = {
            action:'load-personal-inventory',
            controller:'InventoryController',
            output:'JSON',
            data: JSON.stringify(dataValues)
        }

        const url = `/shared/post.php`;
        const response = await fetch(url, {
            method: 'POST',
            mode: "same-origin",
            credentials: 'same-origin',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify(data)
        });
        // if(!checkResponseErrors(response)){
        //     return;
        // }

        const inventoryResponse = await response.json();
        const inventoryData = inventoryResponse.responseData;
        console.log(inventoryData);
    }
})
