window.userWalletAddress = null;

const toggleButton = async () => {
    const metaMaskButton = document.getElementById('metaMaskButton');
    if(!metaMaskButton) { return false; }
    if(!window.ethereum.isMetaMask) {
        metaMaskButton.innerText = "MetaMask is not installed.";
        return false;
    }
    metaMaskButton.addEventListener('click', async () => { await loginWithMetaMask(); });
}

const loginWithMetaMask = async () => {
    const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
    window.userWalletAddress = accounts[0];
    window.localStorage.setItem('userWallet', userWalletAddress);
    showUserWallet();
}

const showUserWallet = () => {
    const userWallet = document.getElementById('userWallet');
    if(!userWallet) { return false; }
    userWallet.innerText = getStoredWallet();
    getContract();
}

const getStoredWallet = () => {
    const walletAddress = window.localStorage.getItem('userWallet');
    return walletAddress;
}

window.addEventListener('DOMContentLoaded', async () => {
    await toggleButton();
    const userWalletStored = window.localStorage.getItem('userWallet');
    if(!userWalletStored) { await loginWithMetaMask(); }
    showUserWallet();
});

const getContract = async () => {
    const contract = new ethers.Contract(getContractAddress(), getABI(), getProvider());
    const bigBal = await contract.balanceOf(getStoredWallet());
    const bal = ethers.utils.formatUnits(bigBal, 18);
    console.log(bal);

}

const getContractAddress = () => {
    const contractAddress = '0xa34519886e2237d7c8c49093754b0f6b948e4305';
    return contractAddress;
}

const getProvider = () => {
    const provider = new ethers.providers.Web3Provider(window.ethereum);
    return provider;
}

const getABI = () => {
    //ERC20 ABI
    const ethABI = [
        {
            "constant": true,
            "inputs": [],
            "name": "name",
            "outputs": [
                {
                    "name": "",
                    "type": "string"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "_spender",
                    "type": "address"
                },
                {
                    "name": "_value",
                    "type": "uint256"
                }
            ],
            "name": "approve",
            "outputs": [
                {
                    "name": "",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [],
            "name": "totalSupply",
            "outputs": [
                {
                    "name": "",
                    "type": "uint256"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "_from",
                    "type": "address"
                },
                {
                    "name": "_to",
                    "type": "address"
                },
                {
                    "name": "_value",
                    "type": "uint256"
                }
            ],
            "name": "transferFrom",
            "outputs": [
                {
                    "name": "",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [],
            "name": "decimals",
            "outputs": [
                {
                    "name": "",
                    "type": "uint8"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [
                {
                    "name": "_owner",
                    "type": "address"
                }
            ],
            "name": "balanceOf",
            "outputs": [
                {
                    "name": "balance",
                    "type": "uint256"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [],
            "name": "symbol",
            "outputs": [
                {
                    "name": "",
                    "type": "string"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "_to",
                    "type": "address"
                },
                {
                    "name": "_value",
                    "type": "uint256"
                }
            ],
            "name": "transfer",
            "outputs": [
                {
                    "name": "",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [
                {
                    "name": "_owner",
                    "type": "address"
                },
                {
                    "name": "_spender",
                    "type": "address"
                }
            ],
            "name": "allowance",
            "outputs": [
                {
                    "name": "",
                    "type": "uint256"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "payable": true,
            "stateMutability": "payable",
            "type": "fallback"
        },
        {
            "anonymous": false,
            "inputs": [
                {
                    "indexed": true,
                    "name": "owner",
                    "type": "address"
                },
                {
                    "indexed": true,
                    "name": "spender",
                    "type": "address"
                },
                {
                    "indexed": false,
                    "name": "value",
                    "type": "uint256"
                }
            ],
            "name": "Approval",
            "type": "event"
        },
        {
            "anonymous": false,
            "inputs": [
                {
                    "indexed": true,
                    "name": "from",
                    "type": "address"
                },
                {
                    "indexed": true,
                    "name": "to",
                    "type": "address"
                },
                {
                    "indexed": false,
                    "name": "value",
                    "type": "uint256"
                }
            ],
            "name": "Transfer",
            "type": "event"
        }
    ];
    return ethABI;
}