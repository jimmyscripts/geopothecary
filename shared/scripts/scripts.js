document.addEventListener('click', async e => {

    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl, {
            trigger: 'hover'
        })
    })

    // Hide any active tooltips when a trigger element is clicked
    tooltipTriggerList.forEach(function (tooltipTriggerEl) {
        tooltipTriggerEl.addEventListener('click', function () {
            var tooltipInstance = bootstrap.Tooltip.getInstance(tooltipTriggerEl);
            if (tooltipInstance) {
                tooltipInstance.hide();
            }
        });
    });

    if(e.target.id == 'inventoryModal') {
        const thisModal = document.getElementById('inventoryModal');
        const modal = bootstrap.Modal.getInstance(thisModal);
        modal.hide();
    }

    if(e.target.id == 'openPersonalInventoryBtn') {
        const id = 'inventoryModal';
        const purpose = 'inventory';

        buildModal(id, purpose);
    }

    if(e.target.id == 'searchForResourcesBtn') {
        await searchForResources();
    }

    if(e.target.classList.contains('resource-pickup')) {

        const locationId = e.target.dataset.location;
        const sourceId = e.target.dataset.source;

        let dataValues = {'locationId' : locationId, 'sourceId' : sourceId};

        const data = {
            action:'pickup-resource-in-location',
            controller:'SearchController',
            output:'JSON',
            data: JSON.stringify(dataValues)
        }

        const url = `/shared/post.php`;
        const response = await fetch(url, {
            method: 'POST',
            mode: "same-origin",
            credentials: 'same-origin',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify(data)
        });
        if(!checkResponseErrorsModal(response)){
            return;
        }

        const sourceResponse = await response.json();
        const collectedSource = sourceResponse.responseData;

        const itemElId = makeId(40);

        const buildInvLocation = collectedSource.inventory_id + '_' + collectedSource.resource_inventory_position;
        const buildCatcherId = 'catcher_' + buildInvLocation;
        const catcherElement= document.getElementById(buildCatcherId);

        const sourceCount = collectedSource.source_count;
        const resourceId = collectedSource.resource_id;
        const sourceLore = collectedSource.source_lore;
        const itemSlug = collectedSource.item_slug;

        const newResource = buildItemElement(collectedSource, 'collected');

        catcherElement.insertAdjacentHTML('afterbegin', newResource);
        setTimeout( buildHeadsUpHotBar, 50 );
        hideModal('generalModal');

        await searchForResources();

    }
});

const buildHeadsUpHotBar = (event) => {
    const hotBarCount = 5;
    for(let i = 0; i <= hotBarCount; i++) {
        const inventoryHotBar = document.getElementById('catcher_hotBar_' + i).firstElementChild;
        if(inventoryHotBar) {
            const copyHotBar = inventoryHotBar.cloneNode(true);
            const headsUpHotBar = document.getElementById('heads_up_hotBar_' + i);
            headsUpHotBar.innerHTML = '';
            headsUpHotBar.appendChild(copyHotBar);
        } else {
            const headsUpHotBar = document.getElementById('heads_up_hotBar_' + i);
            headsUpHotBar.innerHTML = '';
        }
    }
}

window.addEventListener('load', () => {
    setTimeout( buildHeadsUpHotBar, 50 );
});

const searchForResources = async () => {

    const location = await getLocation();
    const lat = location.coords.latitude;
    const long = location.coords.longitude;

    if(lat && long) {
        let dataValues = {'lat' : lat, 'long' : long};

        const data = {
            action:'search-resource-in-location',
            controller:'SearchController',
            output:'JSON',
            data: dataValues
        }

        const url = `/shared/post.php`;
        const response = await fetch(url, {
            method: 'POST',
            mode: "same-origin",
            credentials: 'same-origin',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify(data)
        });

        if(!checkResponseErrors(response)){
            return;
        }

        const searchResponse = await response.json();
        const resourceList = searchResponse.responseData;

        let returnedResources = [];

        if(resourceList['sources'][0] && resourceList['sources'][0].length > 0) {
            returnedResources = pickResource(resourceList['sources'][0]);

            const id = 'generalModal';
            const purpose = 'inventory';
            let body = ``;
            body += `<div class="row text-center justify-content-center">`;
            returnedResources.forEach(function(resource) {
                body += `<div class="col-md-4">
                            <div class="rad-5 thick-border mx-1 p-3 cursor-pointer resource-pickup"
                            data-source="${resource.source_id}"
                            data-location="${resource.location_id}">
                                <div style="pointer-events: none;">
                                    <p>${resource.item_name}</p>
                                    <img style="height: 45px;" src="./shared/svg/${resource.item_slug}.svg" />
                                    <p class="item_counter"><strong>${resource.source_count}</strong></p>
                                </div>
                            </div>
                        </div>`;
            });
            body += `</div>`;

            buildModal(id, purpose, '', body);
        } else if(!resourceList['sources'][0] && resourceList['collected']) {
            let body = ``;
            if(resourceList['nearby']) {
                const nearby = pointClosestLocation(resourceList);

                const latDist = nearby['lat_dist'];
                const longDist = nearby['long_dist'];
                let latSign = Math.sign(latDist);
                let longSign = Math.sign(longDist);

                let latLongRat = 0;
                if(latDist < longDist) {
                    latLongRat = Math.abs(latDist / longDist);
                    if(latLongRat <= 0.25) {
                        latSign = 0;
                    }
                } else if(longDist < latDist) {
                    latLongRat = Math.abs(latDist / longDist);
                    if(latLongRat <= 0.25) {
                        longSign = 0;
                    }
                }

                let arrowRotation = 0;
                const latLongSignKey = latSign + '|' + longSign;

                switch(latLongSignKey) {
                    case '1|1':
                        arrowRotation = 45;
                        break;
                    case '-1|-1':
                        arrowRotation = 225;
                        break;
                    case '1|-1':
                        arrowRotation = 315;
                        break;
                    case '-1|1':
                        arrowRotation = 135;
                        break;
                    case '0|1':
                        arrowRotation = 90;
                        break;
                    case '1|0':
                        arrowRotation = 0;
                        break;
                    case '0|-1':
                        arrowRotation = 270;
                        break;
                    case '-1|0':
                        arrowRotation = 180;
                        break;
                    default:
                        arrowRotation = 0;
                }

                body = `<h4>You have collected everything here today.</h4>
                        <p>Follow the arrow to the next closest source.</p>
                        <div id="directionalContainer">
                            <div id="directional">
                                <div id="arrowContainer" style="transform: rotate(${arrowRotation}deg);">
                                    <div id="directionalArrow"><img src="/shared/svg/up_arrow.svg" /></div>
                                </div>
                            </div>
                        </div>`;
            } else {
                body = `<h4>You have collected everything here today.</h4>`;
            }

            const id = 'generalModal';
            const purpose = 'inventory';

            buildModal(id, purpose, '', body);

        } else if((!resourceList['sources'][0] && !resourceList['collected']) || resourceList['sources'][0].length == 0) {
            let body = ``;
            if(resourceList['nearby']) {
                const nearby = pointClosestLocation(resourceList);

                body = `<h3>There is nothing to collect here.</h3>`;
            } else {
                body = `<h3>There is nothing to collect here.</h3>`;
            }

            const id = 'generalModal';
            const purpose = 'inventory';

            buildModal(id, purpose, '', body);
        }
    } else {
        return;
    }

}

const getLocation = () => new Promise((resolve, reject) => navigator.geolocation.getCurrentPosition(resolve, reject));

const pointClosestLocation = (resourceList) => {
    let distances = [];
    resourceList['nearby'].forEach(function(nearby) {
        let close = [];
        close.push(nearby['distanceToLocation']);
        close.push(nearby['location_id']);
        distances.push(close);
    });

    let lowest = 0;
    for (let i = 0; i < distances.length; i++) {
        if(distances[i][0] <= distances[lowest][0]) {lowest = i}
    }
    return resourceList['nearby'][lowest];
}

const pickResource = (resourceList) => {
    console.log(resourceList);
    let chosenResources = [];
    let rand = 0;
    while(chosenResources.length === 0) {

        rand = Math.floor(Math.pow(Math.random(), 2) * 10 + 1);
        chosenResources = resourceList.filter(resource => resource.source_rarity == rand);
    }
    return chosenResources;
}

const makeId = (length) => {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

const buildItemElement = (newItem, toolName) => {
    let uid = generateUid();

    let count = newItem['result_count'] || newItem['resource_count'];
    count = count || newItem['source_count'];//Well, this needs to get fixed...
    const resourceId = newItem['resource_id'] || 0;
    let lore = newItem['resource_lore'] || newItem['source_lore'];
    lore = lore || '[]';//This too. Source and resource data should have been the same name in db.

    let leftClickPopover = `<div><strong class='btn btn-danger mb-2 float-end closePop'>X</strong></div>`;

    switch (newItem['item_type']) {
        case 'material':
            leftClickPopover += `<div><strong class='btn btn-primary w-100'>Consume</strong></div>`;
            break;
        case 'tool':
            leftClickPopover += `<div><strong class='btn btn-primary w-100'>Install</strong></div>`;
            break;
        default:
            leftClickPopover += ``;
    }

    const itemElement = `<div id="${uid}" class="item" data-item="${newItem['item_slug']}" data-lore="${lore}"
                             data-count="${count}" draggable="true" ondragend="dragend_handler(event)"
                             data-resource-id="${resourceId}" data-item-id="${newItem['item_id']}"
                             data-last-location="${toolName}" data-bs-toggle="popover" title="${newItem['item_name']}"
                                         data-bs-content="${leftClickPopover}" onclick="popover(this)" data-bs-html="true">
                            <img height="45px" id="" class="inventoryItem" data-item="${newItem['item_slug']}"
                                 src="./shared/svg/${newItem['item_slug']}.svg"/>
                            <div class="item_counter"><strong><span id="count_${uid}">${count}</span></strong></div>
                        </div>`;
    return itemElement;
}

const run_tool_function = async (toolName) => {
    const toolForm = document.getElementById(toolName);
    const formData = new FormData(toolForm);
    formData.append('tool', toolName);
    let dataValues = {};
    formData.forEach((value, key) => dataValues[key] = value);

    if(
        !dataValues['tool_slot_0'] &&
        !dataValues['tool_slot_1'] &&
        !dataValues['tool_slot_2'] &&
        !dataValues['tool_slot_3'] &&
        !dataValues['tool_slot_4'] &&
        !dataValues['tool_slot_5'] &&
        !dataValues['tool_slot_6'] &&
        !dataValues['tool_slot_7'] &&
        !dataValues['tool_slot_8']
    ) { deliverNewItem(0); } else {
        const data = {
            action: 'create-new-item',
            controller: 'AlchemyController',
            output: 'JSON',
            data: dataValues
        }

        const url = `/shared/post.php`;
        const response = await fetch(url, {
            method: 'POST',
            mode: "same-origin",
            credentials: 'same-origin',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(data)
        });

        if(!checkResponseErrors(response)){
            return;
        }

        const itemResponse = await response.json();
        const newItem = itemResponse.responseData;

        await deliverNewItem(newItem);
    }
}