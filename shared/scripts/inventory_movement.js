document.addEventListener('dragstart', function(ev) {
    $("[data-bs-toggle='popover']").popover('dispose');
    if(ev.target.classList.contains('item')) {
        dragstart_handler(ev);
    }
});

const dragstart_handler = (ev) => {
    // Add the target element's id to the data transfer object
    ev.dataTransfer.setData("text", ev.target.id);
    ev.dataTransfer.dropEffect = "move";
    ev.target.style.opacity = '0.4';
}

const dragover_handler = (ev) => {
    ev.preventDefault();
    ev.dataTransfer.dropEffect = "move";
}

const drop_handler = async (ev) => {

    ev.preventDefault();

    const slotDroppedNum = split_catcher_slot(ev.target.id);//Get slot number of catcher we dropped item on
    const droppedItemElId = ev.dataTransfer.getData("text");//Get id of the element of the item that we have dropped
    const targetTag = ev.target.tagName ? ev.target.tagName : '';//Get element tag name that we dropped item on
    const targetChildTag = ev.target.firstElementChild ? ev.target.firstElementChild.tagName : '';//Get child tag name of the element we dropped item on
    const droppedItemEl = document.getElementById(droppedItemElId);//Get the item element we have dropped
    const droppedItemId = droppedItemEl.getAttribute('data-item-id');//Get the item id of the item we have dropped
    const droppedResourceId = droppedItemEl.getAttribute('data-resource-id');//Get resource ID of item we are dropping
    const droppedLastLocation = droppedItemEl.getAttribute('data-last-location');
    let droppedCurrentLocation = ev.target.getAttribute('data-tool-name');

    if(!droppedCurrentLocation) {
        const droppedCurrentLocationParent = ev.target.parentNode;
        droppedCurrentLocation = droppedCurrentLocationParent.getAttribute('data-tool-name');
    }

    // Check to see if this table cell of the inventory already has something in it.
    if(targetTag == 'SECTION' && targetChildTag != 'IMG' && targetChildTag != 'DIV') {
        ev.target.appendChild(droppedItemEl);//Add item element to catcher
        const newInventoryLocation = ev.target.getAttribute('data-inventory-location');//Get the new inventory location from catcher
        const targetNewLocation = ev.target.getAttribute('data-tool-name');

        if(droppedLastLocation == 'vessel') {//This is a new item from the tool
            const newItemId = droppedItemEl.getAttribute('data-item-id');//Get new item id
            const newResourceCount = droppedItemEl.getAttribute('data-count');//Get new item count
            const newResourceLore = droppedItemEl.getAttribute('data-lore');//Get new item lore

            const newResourceId = await insert_inventory_location(newInventoryLocation, newItemId, newResourceCount, newResourceLore);//Insert new item into inventory
            droppedItemEl.setAttribute('data-resource-id', newResourceId);
            await empty_tool_slots(newResourceCount);//Check that all tools slots have item counts reduced or items removed because of collected new item
        } else {//We have moved an existing item
            await update_inventory_location(newInventoryLocation, droppedResourceId);
        }
        droppedItemEl.setAttribute('data-last-location', targetNewLocation);
        if(droppedCurrentLocation == 'hotBar' || droppedLastLocation == 'hotBar') {
            setTimeout( buildHeadsUpHotBar, 50 );
        }
    } else {
        const targetItemEl = ev.target;
        const droppedItemName = droppedItemEl.getAttribute('data-item');
        const targetItemName = targetItemEl.getAttribute('data-item');

        // If the occupied inventory cell has the same item, combine them.
        if(targetItemName == droppedItemName) {
            const targetItemElId = targetItemEl.id;
            const droppedItemElId = droppedItemEl.id;

            if(targetItemElId == droppedItemElId) {
                //This should not actually cause anything to happen because each item element should have unique IDs.
            } else {
                const droppedItemLore = droppedItemEl.getAttribute('data-lore');
                const targetItemLore = targetItemEl.getAttribute('data-lore');
                if(droppedItemLore == targetItemLore) {
                    const targetCount = targetItemEl.getAttribute('data-count');
                    const droppedCount = droppedItemEl.getAttribute('data-count');
                    const totalCount = parseInt(targetCount) + parseInt(droppedCount);
                    if(totalCount) {
                        const targetResourceId = targetItemEl.getAttribute('data-resource-id');

                        if(droppedLastLocation == 'vessel') {//This is a new item from the tool
                            const newResourceCount = droppedItemEl.getAttribute('data-count');//Get new item count
                            await update_resource_count(totalCount, targetResourceId, droppedResourceId);
                            await empty_tool_slots(newResourceCount);//Check that all tools slots have item counts reduced or items removed because of collected new item
                        } else {//We have moved an existing item
                            await update_resource_count(totalCount, targetResourceId, droppedResourceId);
                            if(droppedCurrentLocation == 'hotBar' || droppedLastLocation == 'hotBar') {
                                setTimeout( buildHeadsUpHotBar, 50 );
                            }
                        }
                    }
                    targetItemEl.setAttribute('data-count', totalCount.toString());
                    const retrieveCount = targetItemEl.getAttribute('data-count');
                    const updateCount = document.getElementById('count_' + targetItemElId);
                    updateCount.innerText = retrieveCount;
                    droppedItemEl.remove();
                }
            }
        }

        if(droppedCurrentLocation == 'hotBar' || droppedLastLocation == 'hotBar') {
            setTimeout( buildHeadsUpHotBar, 50 );
        }
    }

    //This tells us what tool is open at that time.
    const toolForm = document.getElementsByClassName('tool_form');
    let toolName = '';
    for(let i = 0; i < toolForm.length; i++) {
        //We just need a string of the current tool name.
        toolName = toolForm[i].id;
    }

    if(toolName && (droppedLastLocation == toolName || droppedCurrentLocation == toolName || droppedLastLocation == 'vessel')) {
        await check_empty_slots(toolName);
        await run_tool_function(toolName);
    }
}

const dragend_handler = (ev) => {
    ev.target.style.opacity= '1';
}

const update_resource_count = async (totalCount, targetResourceId, droppedResourceId) => {
    let dataValues = {'totalCount' : totalCount, 'targetResourceId' : targetResourceId, 'droppedResourceId' : droppedResourceId};

    const data = {
        action:'update-resource-count',
        controller:'InventoryController',
        output:'JSON',
        data: JSON.stringify(dataValues)
    }

    const url = `/shared/post.php`;
    const response = await fetch(url, {
        method: 'POST',
        mode: "same-origin",
        credentials: 'same-origin',
        headers: {'Content-Type':'application/json'},
        body: JSON.stringify(data)
    });
    // if(!checkResponseErrors(response)){
    //     return;
    // }

    const inventoryResponse = await response.json();
}

const update_inventory_location = async (toolName, droppedResourceId) => {
    const catcherArray = toolName.split("_");
    const inventoryId = catcherArray[0];
    const positionId = catcherArray[1];

    let dataValues = {'inventoryId' : inventoryId, 'positionId' : positionId, 'droppedResourceId' : droppedResourceId};

    const data = {
        action:'update-resource-position',
        controller:'InventoryController',
        output:'JSON',
        data: dataValues
    }

    const url = `/shared/post.php`;
    const response = await fetch(url, {
        method: 'POST',
        mode: "same-origin",
        credentials: 'same-origin',
        headers: {'Content-Type':'application/json'},
        body: JSON.stringify(data)
    });
    // if(!checkResponseErrors(response)){
    //     return;
    // }

    const inventoryResponse = await response.json();
}

const insert_inventory_location = async (toolName, newItemId, newResourceCount, newResourceLore) => {
    const catcherArray = toolName.split("_");
    const inventoryId = catcherArray[0];
    const positionId = catcherArray[1];

    let dataValues = {'inventoryId' : inventoryId, 'positionId' : positionId, 'newItemId' : newItemId, 'newResourceCount' : newResourceCount, 'newResourceLore' : newResourceLore};

    const data = {
        action:'insert-new-resource',
        controller:'InventoryController',
        output:'JSON',
        data: dataValues
    }

    const url = `/shared/post.php`;
    const response = await fetch(url, {
        method: 'POST',
        mode: "same-origin",
        credentials: 'same-origin',
        headers: {'Content-Type':'application/json'},
        body: JSON.stringify(data)
    });
    if(!checkResponseErrors(response)){
        return;
    }

    const inventoryResponse = await response.json();
    const newInvId = inventoryResponse.responseData;
    return newInvId;
}

const split_catcher_slot = (toolSlot) => {
    const catcherArray = toolSlot.split("_");
    toolSlot = catcherArray.length <= 1 ? 'key' : catcherArray[2] ;
    return toolSlot;
}

const deliverNewItem = (newItem) => {
    const delivery = document.getElementById('vessel');
    let vessel = ``;
    if(newItem && newItem[0]['item_id']) {
        newItem = newItem[0];
        vessel = buildItemElement(newItem, 'vessel');
        delivery.innerHTML = vessel;
    } else {
        delivery.innerHTML = vessel;
    }
}

const check_empty_slots = (toolName) => {
    const tool_slot_count = 9;
    for(let i = 0; i < tool_slot_count; i++) {
        const toolCatcherSlot = document.getElementById('catcher_' + toolName + '_' + i);
        const toolInput = document.getElementById('tool_slot_' + i);
        if(!toolCatcherSlot.hasChildNodes()) {
            toolInput.value = '';
        } else {
            const itemElementOfSlot = toolCatcherSlot.firstChild;
            const itemIdOfSlot = itemElementOfSlot.getAttribute('data-item-id');
            toolInput.value = itemIdOfSlot;
        }
    }
}

const empty_tool_slots = async (sendItemCount) => {
    sendItemCount = 1;//Resetting sendItemCount to 1. Future tools may need the passed value, but that is undetermined.
    const toolSlotsToEmpty = document.getElementsByClassName('tool_catcher_slot');//Get all the slots in tool body

    for(let i = 0; i < toolSlotsToEmpty.length; i++) {//Iterate through all the slots in the tool body
        const delItemId = toolSlotsToEmpty[i].id;//Get the ID of each slot in the tool body
        const catcherArray = delItemId.split("_");
        const inventoryId = catcherArray[1];
        const positionId = catcherArray[2];

        const toolSlotId = 'tool_slot_' + positionId;//Build ID of specific tool slot INPUT element
        const toolSlotEl = document.getElementById(toolSlotId);//Get the tool slot INPUT element
        const toolSlotValue = toolSlotEl.value;//Get the itemId from the specific tool slot INPUT

        if(toolSlotValue && toolSlotsToEmpty[i].firstChild) {//If the itemId from the tool slot INPUT exists AND the tool slot we are in has something in it, we have work to do.
            let toolSlotChild = toolSlotsToEmpty[i].firstChild;//Get the item from inside current tool slot of tool body
            let toolSlotCount = toolSlotChild.getAttribute('data-count');//Get the count of item in current tool slot
            let dataValues = {'inventoryId' : inventoryId, 'positionId' : positionId, 'itemCount' : toolSlotCount};

            const data = {
                action: 'delete-exhausted-resources',
                controller: 'InventoryController',
                output: 'JSON',
                data: dataValues
            }

            const url = `/shared/post.php`;
            const response = await fetch(url, {
                method: 'POST',
                mode: "same-origin",
                credentials: 'same-origin',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(data)
            });

            if (!checkResponseErrors(response)) {
                return;
            }

            if(toolSlotCount <= 1) {
                toolSlotsToEmpty[i].innerHTML = '';
                toolSlotId.value = '';
            } else {
                const newToolSlotCount = toolSlotCount - sendItemCount;
                toolSlotChild.setAttribute('data-count', newToolSlotCount);
                const toolSlotId = toolSlotChild.id
                const updateCount = document.getElementById('count_' + toolSlotId);
                updateCount.innerText = newToolSlotCount;
            }
        }
    }
}