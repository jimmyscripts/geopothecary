const buildModal = (id, purpose = null, header = null, body = null, footer = null, tool = null, position = null, size = null) => {
    const modal = new bootstrap.Modal(document.getElementById(id), true);
    const modalHeader = document.getElementById('modalHeader');
    const modalBody = document.getElementById('modalBody');
    const modalFooter = document.getElementById('modalFooter');
    const modalTool = document.getElementById('modalTool');

    if(purpose) {

    }

    modalHeader.innerHTML = header ? header : '';
    modalBody.innerHTML = body ? body : '';
    modalFooter.innerHTML = footer ? footer : '';
    modalTool.innerHTML = tool ? tool : '';

    if(position) {

    }
    if(size) {

    }

    modal.show();
}
