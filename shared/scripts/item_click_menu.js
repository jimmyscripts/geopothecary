window.onclick = function(event) {
    if(event.target.classList.contains('closePop')) {
        $("[data-bs-toggle='popover']").popover('dispose');
    }
}

const inventoryTableWrapper = document.getElementById('inventoryTableWrapper');
inventoryTableWrapper.onscroll = function(event) {
    $("[data-bs-toggle='popover']").popover('dispose');
}

const popover = (el) => {
    $("[data-bs-toggle='popover']").popover('dispose');
    if (el.dataset.lastLocation == 'personal' || el.dataset.lastLocation == 'hotBar') {
        $(el).popover('toggle');
    } else {
        $("[data-bs-toggle='popover']").popover('dispose');
    }
}

