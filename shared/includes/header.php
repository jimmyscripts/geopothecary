<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>GEOPOTHECARY</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link href="/shared/style.css" rel="stylesheet">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="https://cdn.ethers.io/lib/ethers-5.0.umd.min.js" type="text/javascript"></script>

<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/composer/vendor/autoload.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/models/autoload.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/controllers/autoload.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/shared/includes/helpers.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/shared/inventoryModal.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/shared/modal.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/shared/includes/featureToggles.php');
?>

<script src="/shared/scripts/metamask.js"></script>
<script src="/shared/scripts/helpers.js"></script>
<script src="/shared/scripts/scripts.js"></script>
<script src="/shared/scripts/item_click_menu.js"></script>
<script src="/shared/scripts/context_menu.js"></script>
<script src="/shared/scripts/inventory_movement.js"></script>
<script src="/shared/scripts/inventory_build.js"></script>
<script src="/shared/scripts/open_tools.js"></script>
<script src="/shared/scripts/modal.js"></script>