<?php

$logged_user = "Jimmy";
$body = "<div>Hello, <span>{{personal}}</span>. Welcome, to this tutorial.</div>";
$new_body = str_replace("{{personal}}", $logged_user, $body);
echo $new_body;
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light" style="z-index: 10000">
    <div class="container-fluid">
        <a class="navbar-brand" href="#"><img src="/shared/svg/blueberry.svg" alt="" width="45"></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/">Home</a>
                </li>
                <?php if(!$_SESSION['username']) : ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/register">Register</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/login">Login</a>
                    </li>
                <?php else : ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/logout?logout=logout">Logout</a>
                    </li>
                <?php endif; ?>
                    <?php if($metaMaskToggle) : ?>
                        <li class="nav-item">
                            <a id="metaMaskButton" class="nav-link" href="#">MetaMask</a>
                        </li>
                        <li class="nav-item">
                            <a id="userWallet" class="nav-link" href="#"></a>
                        </li>
                    <?php endif; ?>

                    <?php if($_SESSION['username'] == 'jimmyscripts') : ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/load_locations">Load Locations</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/view_locations">View Locations</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/add_resources">Add Resources</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/add_locations">Add Locations</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/utility_dashboard">Utility Dashboard</a>
                        </li>
                    <?php endif; ?>
<!--                <li class="nav-item dropdown">-->
<!--                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"-->
<!--                       data-bs-toggle="dropdown" aria-expanded="false">-->
<!--                        Dropdown-->
<!--                    </a>-->
<!--                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">-->
<!--                        <li><a class="dropdown-item" href="#">Action</a></li>-->
<!--                        <li><a class="dropdown-item" href="#">Another action</a></li>-->
<!--                        <li>-->
<!--                            <hr class="dropdown-divider">-->
<!--                        </li>-->
<!--                        <li><a class="dropdown-item" href="#">Something else here</a></li>-->
<!--                    </ul>-->
<!--                </li>-->
            </ul>
            <form class="d-flex">
                <?php if($_SESSION['username']) : ?>
                    <button id="searchForResourcesBtn" class="btn btn-outline-primary search-resources me-2" type="button">Forage</button>
                    <button id="openPersonalInventoryBtn" class="btn btn-outline-success inventory" type="button">Inventory</button>
                <?php else : ?>
                <?php endif; ?>
            </form>
        </div>
    </div>
</nav>
