<table id="headsUpHotBar" class="text-center" <?php if(!$hotBarToggle) : ?> style="filter: opacity(0);" <?php endif; ?>>
    <tbody>
        <tr>
            <td style="width:16.667%;">
                <section id="heads_up_hotBar_0">
                </section>
            </td>
            <td style="width:16.667%;">
                <section id="heads_up_hotBar_1">
                </section>
            </td>
            <td style="width:16.667%;">
                <section id="heads_up_hotBar_2">
                </section>
            </td>
            <td style="width:16.667%;">
                <section id="heads_up_hotBar_3">
                </section>
            </td>
            <td style="width:16.667%;">
                <section id="heads_up_hotBar_4">
                </section>
            </td>
            <td style="width:16.667%;">
                <section id="heads_up_hotBar_5">
                </section>
            </td>
        </tr>
    </tbody>
</table>