<nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="z-index: 10000">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <span class="nav-link active" aria-current="page" style="height: 40px;"><?php if($toolBarToggle) : ?>Tool Bar:<?php endif; ?></span>
                </li>
            </ul>
            <?php if($toolBarToggle) : ?>
                <form class="d-flex">
                    <?php if($_SESSION['username']) : ?>
                        <button id="openMixingBowl" class="btn btn-outline-primary me-2" type="button">Mixing Bowl</button>
                        <button id="" class="btn btn-outline-success" type="button">Undefined</button>
                    <?php else : ?>
                    <?php endif; ?>
                </form>
            <?php endif; ?>
        </div>
    </div>
</nav>
