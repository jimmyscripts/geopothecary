<?php

class InventoryController extends Controller {
    public function __construct($values) {
        $this->values = $values;
        $this->data = $values['data'];
    }

    public function getResponse() {
        switch($this->values['action']) {
            case 'load-personal-inventory':
                return $this->getResponseOutput($this->loadPersonalInventory());
            case 'update-resource-count':
                return $this->getResponseOutput($this->updateResourceCount());
            case 'update-resource-position':
                return $this->getResponseOutput($this->updateResourcePosition());
            case 'insert-new-resource':
                return $this->getResponseOutput($this->insertNewResource());
            case 'delete-exhausted-resources':
                return $this->getResponseOutput($this->deleteExhaustedResources());
            default:
                return '';
        }
    }

    /**
     * Returns personal inventory including hotbar
     *
     * @return object $inventoryFactory
     */
    private function loadPersonalInventory():object {
        $data = json_decode($this->values['data']);

        $inventoryFactory = new InventoryFactory();
        $createInventory = $inventoryFactory->createInventory($data->userId, 'personal');

        return $createInventory;
    }

    /**
     * Updates resource counts after being stacked
     *
     * @return null
     */
    private function updateResourceCount() {
        $data = json_decode($this->values['data']);

        $inventory = new Inventory();
        $inventory->saveResourceCount($data->totalCount, $data->targetResourceId, $data->droppedResourceId);
    }

    /**
     * Updates resource inventory position
     *
     * @return null
     */
    private function updateResourcePosition() {
        $inventory = new Inventory();
        $inventory->saveResourcePosition($this->data['inventoryId'], $this->data['positionId'], $this->data['droppedResourceId']);
    }

    /**
     * Inserts a new resource
     *
     * @return int
     */
    private function insertNewResource():int {
        $inventory = new Inventory();
        return $inventory->saveNewResource(
            $this->data['inventoryId'],
            $this->data['positionId'],
            $this->data['newItemId'],
            $this->data['newResourceCount'],
            $this->data['newResourceLore']
        );
    }

    /**
     * Deletes exhausted resources used in alchemy
     *
     * @return null
     */
    private function deleteExhaustedResources() {
        $inventory = new Inventory();
        $inventory->removeExhaustedResources(
            $this->data['inventoryId'],
            $this->data['positionId'],
            $this->data['itemCount']
        );
    }
}

