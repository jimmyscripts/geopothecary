<?php

class ControllerFactory {
    public function __construct($values) {
        $this->controller = $values['controller'];
        $this->values = $values;
    }

    public function goController() {
        switch($this->controller) {
            case 'InventoryController':
                return new InventoryController($this->values);
            case 'StorageController':
                return new StorageController($this->values);
            case 'SearchController':
                return new SearchController($this->values);
            case 'UserController':
                return new UserController($this->values);
            case 'ToolController':
                return new ToolController($this->values);
            case 'AlchemyController':
                return new AlchemyController($this->values);
            case 'UtilityController':
                return new UtilityController($this->values);
            default:
                return '';
        }
    }
}
