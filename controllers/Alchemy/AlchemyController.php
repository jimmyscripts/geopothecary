<?php

class AlchemyController extends Controller {
    public function __construct($values) {
        $this->values = $values;
    }

    public function getResponse() {
        switch($this->values['action']) {
            case 'create-new-item':
                return $this->getResponseOutput($this->createNewItem());
            default:
                return '';
        }
    }

    private function createNewItem() {
        $data = $this->values['data'];

        $alchemy = new Alchemy();
        return $alchemy->setMixture($data['tool'], $data);
    }
}

?>