<?php

function autoloadGeoControllers($className) {

    $classesDir = $_SERVER['DOCUMENT_ROOT'] . '/controllers';
    $filename = $className . '.php';

    if(is_readable($classesDir.'/'.$filename)) {
        require_once $classesDir.'/'.$filename;
    }
    if (is_readable($classesDir."/Database/".$filename)) {
        require_once $classesDir."/Database/".$filename;
    }
    if (is_readable($classesDir."/Inventory/".$filename)) {
        require_once $classesDir."/Inventory/".$filename;
    }
    if (is_readable($classesDir."/Search/".$filename)) {
        require_once $classesDir."/Search/".$filename;
    }
    if (is_readable($classesDir."/User/".$filename)) {
        require_once $classesDir."/User/".$filename;
    }
    if (is_readable($classesDir."/Tool/".$filename)) {
        require_once $classesDir."/Tool/".$filename;
    }
    if (is_readable($classesDir."/Alchemy/".$filename)) {
        require_once $classesDir."/Alchemy/".$filename;
    }
    if (is_readable($classesDir."/Utility/".$filename)) {
        require_once $classesDir."/Utility/".$filename;
    }
}
spl_autoload_register('autoloadGeoControllers');

?>