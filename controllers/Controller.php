<?php
class Controller
{
    protected $warningsArr = [];

    protected function getResponseOutput($responseData) {
        $warningMsgArr = $this->getWarningMessages();

        if ($this->values['output'] == 'JSON') {
            $this->returnJson($responseData, $warningMsgArr);
        } else {
            return $this->returnArr($responseData, $warningMsgArr);
        }
    }

    protected function returnJson($responseData, $warningMsgArr) {
        echo json_encode([
           'responseData' => $responseData,
           'warnings' => $warningMsgArr
        ]);
    }

    protected function returnArr($responseData, $warningMsgArr) {
        return array('responseData' => $responseData, 'warnings' => $warningMsgArr);
    }

    public function getWarningMessages() {
        return array_map(function(GEOWarning $warning) {
            return $warning->getMessage();
        }, $this->warningsArr);
    }

    protected function addWarning(GEOWarning $warning) {
        $this->warningsArr[] = $warning;
    }
}
?>