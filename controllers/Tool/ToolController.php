<?php

class ToolController extends Controller {
    public function __construct($values) {
        $this->values = $values;
    }

    public function getResponse() {
        switch($this->values['action']) {
            case 'get-tool-data':
                return $this->getResponseOutput($this->getToolData());
            default:
                return '';
        }
    }

    private function getToolData() {
        $data = json_decode($this->values['data']);

        $tool = new Tool();
        return $tool->setToolData($data->tool);
    }
}