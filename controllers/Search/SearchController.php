<?php

class SearchController extends Controller {
    public function __construct($values) {
        $this->values = $values;
    }

    public function getResponse() {
        switch($this->values['action']) {
            case 'search-resource-in-location':
                return $this->getResponseOutput($this->searchResourceInLocation());
            case 'pickup-resource-in-location':
                return $this->getResponseOutput($this->pickupResourceInLocation());
            case 'search-resources-for-map':
                return $this->getResponseOutput($this->searchResourcesForMap());
            default:
                return '';
        }
    }

    /**
     * Send search data to search model
     *
     * @return array
     * @throws GEOServerException
     */
    private function searchResourceInLocation():array {
        $search = new SearchFactory();
        return $search->setSourceData($this->values);
    }

    /**
     * Send pickup data to search model
     *
     * @return array
     */
    private function pickupResourceInLocation():array {
        $data = json_decode($this->values['data']);
        $locationId = (int)$data->locationId;
        $sourceId = (int)$data->sourceId;

        $search = new Search();
        return $search->setPickupData($locationId, $sourceId);
    }

    /**
     * Send search data to search model to populate map
     *
     * @return array
     * @throws GEOServerException
     */
    private function searchResourcesForMap():array {
        $data = $this->values['data'];

        $search = new SearchFactory();
        return $search->setMapData($data['lat'], $data['lng']);
    }
}

?>