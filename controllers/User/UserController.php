<?php

class UserController extends Controller {
    public function __construct($values) {
        $this->values = $values;
    }

    public function getResponse() {
        switch($this->values['action']) {
            case 'register-user':
                return $this->getResponseOutput($this->registerUser());
            case 'login-user':
                return $this->getResponseOutput($this->loginUser());
            case 'logout-user':
                return $this->getResponseOutput($this->logoutUser());
            default:
                return '';
        }
    }

    private function registerUser() {
        $data = json_decode($this->values['data']);

        if(isset($data->email)) {
            $user = new User();
            $user->registerUser($data->email, $data->pass, $data->cpass, $data->username);
        }
    }

    private function loginUser() {
        $data = json_decode($this->values['data']);

        if(isset($data->login)) {
            $user = new User();
            $user->loginUser($data->login, $data->pass);
        }
    }

    private function logoutUser() {
        $data = json_decode($this->values['data']);

        if(isset($data->logout)) {
            $user = new User();
            $user->logoutUser();
        }
    }
}

?>
