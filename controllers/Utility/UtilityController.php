<?php

class UtilityController extends Controller {
    public function __construct($values) {
        $this->values = $values;
        $this->data = $values['data'];
    }

    public function getResponse() {
        switch($this->values['action']) {
            case 'create-place':
                return $this->getResponseOutput($this->createPlace());
            case 'add-new-resource':
                return $this->getResponseOutput($this->addNewResource());
            case 'add-new-location':
                return $this->getResponseOutput($this->addNewLocation());
            case 'run-default-list-builder':
                return $this->getResponseOutput($this->runDefaultListBuilder());
            case 'run-default-location-builder':
                return $this->getResponseOutput($this->runDefaultLocationBuilder());
            case 'run-default-source-builder':
                return $this->getResponseOutput($this->runDefaultSourceBuilder());
            default:
                return '';
        }
    }

    /**
     * Create new location from
     */
    private function createPlace() {
            $utility = new Utility();
            $utility->savePlace(
                $this->data['location_lat'],
                $this->data['location_long'],
                $this->data['google_name'],
                $this->data['google_address'],
                $this->data['google_types'],
                $this->data['google_queries']
            );
    }

    /**
     * Add new resource
     */
    private function addNewResource() {
        $utility = new Utility();
        $utility->addNewResource(
            $this->data['locationId'],
            $this->data['itemSelect'],
            $this->data['itemCount'],
            $this->data['rarity']
        );
    }

    /**
     * Add new location
     */
    private function addNewLocation() {
        $utility = new Utility();
        $utility->addNewLocation(
            $this->data['location_lat'],
            $this->data['location_long'],
            $this->data['location_name'],
            $this->data['location_address']
        );
    }

    /**
     * Run default item builder
     */
    private function runDefaultListBuilder() {
        $utility = new Utility();
        $utility->runDefaultListBuilder();
    }

    /**
     * Run default location builder
     */
    private function runDefaultLocationBuilder() {
        $utility = new Utility();
        $utility->runDefaultLocationBuilder();
    }

    /**
     * Run default source builder
     */
    private function runDefaultSourceBuilder() {
        $utility = new Utility();
        $utility->runDefaultSourceBuilder();
    }
}